package jp.sunnygem.sumaiful

import android.content.Context
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import com.squareup.picasso.Callback
import jp.sunnygem.sumaiful.Client
import jp.sunnygem.sumaiful.R
import jp.wasabeef.picasso.transformations.MaskTransformation
import android.graphics.pdf.PdfRenderer
import android.graphics.Bitmap
import android.os.ParcelFileDescriptor
import android.os.Build
import android.support.annotation.RequiresApi
import java.io.File


open class URLImageView2 : android.support.v7.widget.AppCompatImageView, Callback {

    protected val mProgressBar = ProgressBar(context)
    protected var isLoading = false

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    private fun setup() {
        if (mProgressBar.parent == null) {
            val parent = parent
            if (parent is ConstraintLayout) {
                parent.addView(mProgressBar)
                val params = mProgressBar.layoutParams as ConstraintLayout.LayoutParams
                params.startToStart = id
                params.endToEnd = id
                params.topToTop = id
                params.bottomToBottom = id
                params.horizontalBias = 0.5f
                params.verticalBias = 0.5f
                mProgressBar.layoutParams = params
                if (!isLoading)
                    mProgressBar.visibility = View.INVISIBLE
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        setup()
    }

    open fun cancel(){
        setImageURI(null)
        isLoading = false
    }


    open fun loadUrl(url: String?, fit:Boolean = true, roundCorner:Boolean = false) {

        setImageURI(null)


        url?.let {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (it.endsWith("pdf")) {
                    isLoading = true
                    mProgressBar.alpha = 1.0f
                    mProgressBar.visibility = View.VISIBLE

                    ImageLoader.instance.loadPdf(it){
                        if(isLoading)
                            mProgressBar.animate().alpha(0f).setDuration(200).withEndAction { mProgressBar.visibility = View.INVISIBLE }
                                .start()

                        isLoading = false
                        setImageBitmap(it)
                    }
                    return
                }
            }


            if(url.startsWith("android.resource://")){
                setImageURI(Uri.parse(url))
                return
            }

            isLoading = true
            mProgressBar.alpha = 1.0f
            mProgressBar.visibility = View.VISIBLE
            if(roundCorner){
                val transformation = MaskTransformation(context, R.drawable.corner_image_mask)
                if (fit) Client.picasso?.load(it)?.fit()?.centerInside()?.transform(transformation)?.into(this, this)
                else Client.picasso?.load(it)?.transform(transformation)?.into(this, this)
            }
            else {
                if (fit) Client.picasso?.load(it)?.fit()?.centerInside()?.into(this, this)
                else Client.picasso?.load(it)?.into(this, this)
            }
            return
        }
        isLoading = false
        setImageDrawable(null)
    }

    override fun onSuccess() {
        if(isLoading)
            mProgressBar.animate().alpha(0f).setDuration(200).withEndAction { mProgressBar.visibility = View.INVISIBLE }
                .start()

        isLoading = false
    }

    override fun onError(e: Exception) {
        if(isLoading)
            mProgressBar.animate().alpha(0f).setDuration(200).withEndAction { mProgressBar.visibility = View.INVISIBLE }
            .start()

        isLoading = false
    }
}
