package jp.sunnygem.sumaiful;

import android.content.Context;
import android.util.AttributeSet;

public class HeightBasedAspectRatioImageView extends URLImageView2 {
    public HeightBasedAspectRatioImageView(Context context) {
        super(context);
    }

    public HeightBasedAspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeightBasedAspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (getDrawable() == null)
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        else {
            int h = MeasureSpec.getSize(heightMeasureSpec);
            int w = h * getDrawable().getIntrinsicWidth() / getDrawable().getIntrinsicHeight();
            setMeasuredDimension(w, h);
        }
    }
}
