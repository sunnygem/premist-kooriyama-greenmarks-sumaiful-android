package jp.sunnygem.sumaiful.Tab.Concierge;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabRootFragment;

public class ConciergeFragment extends TabRootFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setUseDefaultHeader(false);
        return inflater.inflate(R.layout.view_fragment_concierge, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void reset(){
        reset(new MainFragment());
    }

}
