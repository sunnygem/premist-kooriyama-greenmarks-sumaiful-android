package jp.sunnygem.sumaiful.Tab;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.R;

public class TabRootFragment extends Fragment {

    List<Fragment> mStack = new ArrayList<>();
    ImageView mHeaderImageView;
    boolean useDefaultHeader = true;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHeaderImageView = view.findViewById(R.id.image_header);
        reset();
    }

    public void setUseDefaultHeader(boolean flg){
        useDefaultHeader = flg;
    }

    void hideHeader(){
        if(useDefaultHeader) {
            mHeaderImageView.setImageDrawable(getResources().getDrawable(R.mipmap.title_login_logo));
            getView().findViewById(R.id.image_logo).setVisibility(View.GONE);
        }
    }
    void showHeader(){
        if(useDefaultHeader) {
            mHeaderImageView.setImageDrawable(getResources().getDrawable(R.mipmap.title_login_logo));
            getView().findViewById(R.id.image_logo).setVisibility(View.VISIBLE);
        }
    }


    public void reset(){

    }

    public void willAppear(){
        Fragment currentFragment = null;
        if(!mStack.isEmpty()) currentFragment = mStack.get(mStack.size()-1);
        if(currentFragment != null && currentFragment instanceof TabSubFragment){
            TabSubFragment f = (TabSubFragment) currentFragment;
            f.willAppear();
        }
    }

    public void pushFragment(Fragment fragment){
        Fragment currentFragment = null;
        if(!mStack.isEmpty()) currentFragment = mStack.get(mStack.size()-1);
        if(currentFragment == fragment) return;


        TabSubFragment sub = null;
        if(fragment instanceof TabSubFragment){
            sub = (TabSubFragment)fragment;

            sub.setParent(this);
        }

        mStack.add(fragment);

        FragmentManager fragmentManager = this.getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.sub_root, fragment);


        fragmentTransaction.commit();
        if(sub != null) {
            if (sub.usesHeader()) showHeader();
            else hideHeader();
        }

    }
    public void popToRootFragment(){
        if(mStack.size() <= 1) return;
        Fragment next = null;
        while(true) {
            mStack.remove(mStack.size() - 1);

            if (!mStack.isEmpty()) {
                next = mStack.get(mStack.size() - 1);
            }
            if(mStack.size() == 1) break;
        }

        if(next != null) {
            FragmentManager fragmentManager = this.getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.sub_root, next);
            fragmentTransaction.commit();

            if(next instanceof TabSubFragment) {
                TabSubFragment sub = (TabSubFragment) next;
                if (sub.usesHeader()) showHeader();
                else hideHeader();
            }

        }
    }

    public boolean canPopBack(){
        return !mStack.isEmpty();
    }

    public void popFragment(){
        if(mStack.isEmpty()) return;
        mStack.remove(mStack.size()-1);

        Fragment next = null;
        if(!mStack.isEmpty()){
            next = mStack.get(mStack.size()-1);
        }

        if(next != null) {
            FragmentManager fragmentManager = this.getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.sub_root, next);
            fragmentTransaction.commit();

            if(next instanceof TabSubFragment) {
                TabSubFragment sub = (TabSubFragment) next;
                if (sub.usesHeader()) showHeader();
                else hideHeader();
            }

        }
    }

    protected void reset(Fragment fragment){
        mStack.clear();
        pushFragment(fragment);
    }

}
