package jp.sunnygem.sumaiful.Tab.Concierge;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.IndicatedRecyclerView;
import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.Model.ManualCategory;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;

public class ManualCategoryFragment extends TabSubFragment {

    ManualCategoryAdapter mAdapter = new ManualCategoryAdapter();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_manual_category, container, false);
    }


    private void reload(){
        View view1 = getView();
        final IndicatedRecyclerView listView = view1.findViewById(R.id.list_category);

        listView.setLoading(true);
        Client.INSTANCE.fetchManualCategories((concierges, e) -> {
            Activity activity = getActivity();
            if(activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.clear();
                        for (ManualCategory c : concierges) {
                            mAdapter.addCategory(c);
                        }
                        listView.setLoading(false);
                    }
                });
            }
            return null;
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View view1 = getView();
        IndicatedRecyclerView list = view1.findViewById(R.id.list_category);

        mAdapter.setOnSelectListener(new jp.sunnygem.sumaiful.Tab.HomeKit.ListAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                openConcierge(mAdapter.getCategory(index));
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        list.setLayoutManager(gridLayoutManager);
        list.setAdapter(mAdapter);
        list.setItemAnimator(new FadeInAnimator());

        ImageButton back = view.findViewById(R.id.button_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.popFragment();
            }
        });


        reload();
    }

    void openConcierge(ManualCategory a){

        Fragment nextFragment = null;

        SpecsFragment specs = new SpecsFragment();
        specs.setManualCategory(a);
        nextFragment = specs;


        if(nextFragment != null) {
            mParent.pushFragment(nextFragment);
        }
    }


}
