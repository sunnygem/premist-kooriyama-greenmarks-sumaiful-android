package jp.sunnygem.sumaiful.Tab.Concierge;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.sunnygem.sumaiful.Model.InputSurvey;
import jp.sunnygem.sumaiful.Model.NumberSelectSurvey;
import jp.sunnygem.sumaiful.Model.SelectSurvey;
import jp.sunnygem.sumaiful.Model.Survey;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class SurveyListFragment extends TabSubFragment {


    private SurveyListAdapter mAdapter = new SurveyListAdapter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_survey_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SelectSurvey s1 = new SelectSurvey("Title1", "2018.05.12", false);
        s1.addSelection("Test1");
        s1.addSelection("Test2");
        s1.addSelection("Test3");
        mAdapter.addSurvey(s1);

        SelectSurvey s2 = new SelectSurvey("Title2", "2018.05.12", true);
        s2.addSelection("Test1");
        s2.addSelection("Test2");
        s2.addSelection("Test3");
        mAdapter.addSurvey(s2);

        mAdapter.addSurvey(new NumberSelectSurvey("Title3", "2018.05.12", 10));
        mAdapter.addSurvey(new NumberSelectSurvey("Title4", "2018.05.12", 10));
        mAdapter.addSurvey(new InputSurvey("Title5", "2018.05.12"));

        RecyclerView list = view.findViewById(R.id.list);
        list.setAdapter(mAdapter);
        list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));


        mAdapter.setOnSelectListener(new SelectViewAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                Survey s = mAdapter.getSurvey(index);
                openSurvey(s);
            }
        });
    }



    public void openSurvey(Survey s){
        SurveyRootFragment fragment = null;
        if(s instanceof SelectSurvey){
            SelectSurvey ss = (SelectSurvey) s;
            if(ss.canMultiSelect()) fragment = new SurveyMultiSelectFragment();
            else fragment = new SurveySelectFragment();
        }
        else if(s instanceof InputSurvey){
            fragment = new SurveyInputFragment();
        }

        if(fragment != null) {
            fragment.setSurvey(s);
            mParent.pushFragment(fragment);
        }
    }
}
