package jp.sunnygem.sumaiful.Tab;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import jp.sunnygem.sumaiful.R;

public class TabBarItemView extends RelativeLayout {

    public interface ItemClickListener{
        void didClickTab(int index);
    };

    ItemClickListener mClickListener;

    ImageButton mButon;
    TextView mBadge;

    Drawable mSelectedImage;
    Drawable mUnselectedImage;
    int mIndex;

    public TabBarItemView(Context context) {
        super(context);
    }

    public TabBarItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TabBarItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TabBarItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setup(int index, ItemClickListener clickListener){
        mButon = (ImageButton)findViewById(R.id.button_tab);
        mBadge = (TextView)findViewById(R.id.badge);
        mIndex = index;
        mClickListener = clickListener;
        mButon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.didClickTab(mIndex);
            }
        });
        mBadge.setVisibility(View.INVISIBLE);
    }

    public void setImage(Drawable selectedImage, Drawable unselectedImage){
        mSelectedImage = selectedImage;
        mUnselectedImage = unselectedImage;
        mButon.setImageDrawable(unselectedImage);
    }


    public void setBadge(String text){
        if(text != null) {
            mBadge.setVisibility(View.VISIBLE);
            mBadge.setText(text);
        }
        else{
            mBadge.setVisibility(View.INVISIBLE);
        }
    }

    public void select(boolean flag){
        if(flag){
            mButon.setImageDrawable(mSelectedImage);
        }
        else{
            mButon.setImageDrawable(mUnselectedImage);
        }
    }



}
