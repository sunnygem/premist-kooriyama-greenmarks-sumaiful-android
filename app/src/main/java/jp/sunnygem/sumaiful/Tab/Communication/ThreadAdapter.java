package jp.sunnygem.sumaiful.Tab.Communication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Model.Communication;
import jp.sunnygem.sumaiful.R;

public class ThreadAdapter extends RecyclerView.Adapter<ThreadAdapter.ViewHolder> {


    Communication.Thread mThread;
    private boolean mIsReloading = false;
    private int mLastDataSize = 0;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View inflate = null;
        if(viewType == 0){
            inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_communication_talk, parent, false);
        }
        else {
           inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_communication_talk_incoming, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }


    @Override
    public int getItemViewType(int position) {
        if(mThread.getMessages().get(position).isIncoming() == true) return 1;
        return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mThread.getMessages().get(position));
    }

    @Override
    public int getItemCount() {
        if(mIsReloading) return 0;
        return mThread.getMessages().size();
    }

    public void willReloadData(){
        mLastDataSize = mThread.getMessages().size();
    }

    public void reloadData(){
        int size = mThread.getMessages().size();
        if(mLastDataSize > 0) {
            mIsReloading = true;
            notifyItemRangeRemoved(0, mLastDataSize - 1);
            mIsReloading = false;
        }
        notifyItemRangeInserted(0, size);
    }

    void setThread(Communication.Thread thread){
        mThread = thread;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mIconView;
        TextView mContentLabel;
        TextView mDateLabel;
        TextView mUserNameLabel;


        public ViewHolder(View itemView) {
            super(itemView);
            mIconView = itemView.findViewById(R.id.image_icon);
            mContentLabel = itemView.findViewById(R.id.label_title);
            mDateLabel = itemView.findViewById(R.id.label_date);
            mUserNameLabel = itemView.findViewById(R.id.label_username);
        }

        public void bind(Communication.Message data){
            mContentLabel.setText(data.getContents());
            mDateLabel.setText(data.getDate());
        }
    }

}
