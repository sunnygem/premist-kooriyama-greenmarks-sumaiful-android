package jp.sunnygem.sumaiful.Tab.Communication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.IndicatedRecyclerView;
import jp.sunnygem.sumaiful.Model.Communication;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class ThreadListFragment extends TabSubFragment {


    Communication mCommunication;
    ThreadListAdapter mAdapter = new ThreadListAdapter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_communication_list, container, false);
    }

    public void setCommunication(Communication communication) {
        mCommunication = communication;
    }

    void reload(){

        View view1 = getView();
        final IndicatedRecyclerView listView = view1.findViewById(R.id.list);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View root = getView();
        RecyclerView list = root.findViewById(R.id.list);
        list.getRecycledViewPool().clear();
        mAdapter.setOnSelectListener(new ThreadListAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                openThread(mCommunication.getThreads().get(index));
            }
        });

        mAdapter.setCommunication(mCommunication);
//        list.setItemAnimator(new FadeInAnimator());
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(mAdapter);

        reload();
    }

    void openThread(Communication.Thread th){
        ThreadFragment talk = new ThreadFragment();
        talk.setThread(th);
        mParent.pushFragment(talk);
    }
}
