package jp.sunnygem.sumaiful.Tab.Concierge;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.IndicatedRecyclerView;
import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.Model.ManualCategory;
import jp.sunnygem.sumaiful.Model.Specs;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

import java.util.ArrayList;

public class SpecsFragment extends TabSubFragment {


    private ManualCategory mManualCategory = null;
    private SpecsListAdapter mAdapter = new SpecsListAdapter();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_specs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter.setOnSelectListener(new SelectViewAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                ArrayList<String> files = mAdapter.manual(index).getFiles();
                if(!files.isEmpty()) openURL(files.get(0));
            }
        });
        reload();
        IndicatedRecyclerView list = view.findViewById(R.id.list);
        list.setAdapter(mAdapter);
        list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        ImageButton back = view.findViewById(R.id.button_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.popFragment();
            }
        });

    }

    private void reload(){
        if(mManualCategory == null) return;
        View view = getView();
        IndicatedRecyclerView list = view.findViewById(R.id.list);

        list.setLoading(true);
        Client.INSTANCE.fetchManuals(mManualCategory, (concierges, e) -> {
            Activity activity = getActivity();
            if(activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.clear();
                        for (Concierge.Manual c : concierges) {
                            mAdapter.addManual(c);
                        }
                        list.setLoading(false);
                    }
                });
            }
            return null;
        });
    }

    public void setManualCategory(ManualCategory manualCategory) {
        this.mManualCategory = manualCategory;
    }
}
