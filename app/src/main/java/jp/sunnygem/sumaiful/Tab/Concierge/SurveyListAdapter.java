package jp.sunnygem.sumaiful.Tab.Concierge;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Model.Survey;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;

public class SurveyListAdapter extends SelectViewAdapter<SurveyListAdapter.ViewHolder> {

    private List<Survey> mList = new ArrayList<>();

    @NonNull
    @Override
    public SurveyListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_survey, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.bind(mList.get(position));
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitleLabel;
        private TextView mDateLabel;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleLabel = itemView.findViewById(R.id.label_title);
            mDateLabel = itemView.findViewById(R.id.label_date);
        }


        public void bind(Survey data){
            mTitleLabel.setText(data.getTitle());
            mDateLabel.setText(data.getDate() + " UP");
        }
    }

    public void addSurvey(Survey s){
        mList.add(s);
    }

    public Survey getSurvey(int i){
        return mList.get(i);
    }
}
