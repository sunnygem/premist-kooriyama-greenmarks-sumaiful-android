package jp.sunnygem.sumaiful.Tab.Concierge;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Model.NumberSelectSurvey;
import jp.sunnygem.sumaiful.Model.SelectSurvey;
import jp.sunnygem.sumaiful.R;

public class SurveySelectAdapter extends RecyclerView.Adapter<SurveySelectAdapter.ViewHolder> {


    private SelectSurvey mSurvey;
    private Context mContext;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder = null;
        if(mSurvey.canMultiSelect()){
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_survey_selection_multi, parent, false);
            viewHolder = new ViewHolder(inflate);
        }
        else{
            View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_survey_selection, parent, false);
            viewHolder = new ViewHolder(inflate);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mSurvey.getSelection(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectSurvey.Selection selection = mSurvey.getSelection(position);
                if(selection.isSelected()){
                    mSurvey.getSelection(position).setSelected(false);
                }
                else {
                    if (!mSurvey.canMultiSelect()) {
                        int cnt = mSurvey.getSelectionCount();
                        for (int i = 0; i < cnt; ++i) {
                            mSurvey.getSelection(i).setSelected(false);
                        }
                    }
                    mSurvey.getSelection(position).setSelected(true);
                }
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mSurvey.getSelectionCount();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitleLabel;
        private ImageView mcheckImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleLabel = itemView.findViewById(R.id.label_title);
            mcheckImageView = itemView.findViewById(R.id.image_select);
        }


        public void bind(SelectSurvey.Selection selection){
            mTitleLabel.setText(selection.getTitle());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                if(mSurvey instanceof NumberSelectSurvey){
                    mTitleLabel.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }
                else{
                    mTitleLabel.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                }
            }

            if(mcheckImageView != null){
                if (selection.isSelected()) {
                    mcheckImageView.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_survey_check));
                } else {
                    mcheckImageView.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_survey_uncheck));
                }
            }
            else {
                if (selection.isSelected()) {
                    itemView.setBackgroundColor(Color.argb(128, 0, 0, 0));
                } else {
                    itemView.setBackgroundColor(Color.argb(0, 0, 0, 0));
                }
            }
        }
    }

    public SelectSurvey getSurvey() {
        return mSurvey;
    }

    public void setSurvey(SelectSurvey survey) {
        mSurvey = survey;
    }

    public void setContext(Context context) {
        mContext = context;
    }
}
