package jp.sunnygem.sumaiful.Tab.Concierge;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.*;
import android.widget.Button;
import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class SurveyWebFragment extends TabSubFragment {

    private Concierge.Contacts mContact = null;
    private WebView mWebView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_survey_web, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mWebView = view.findViewById(R.id.view_web);

        mWebView.clearCache(true);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);


        mWebView.setWebViewClient(new WebViewClient(){
            private String mMessage = "";

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
//                Log.getInstance().debug(TAG, "onReceivedHttpAuthRequest() host:" + host + " handler:" + handler);
                //super.onReceivedHttpAuthRequest(view, handler, host, realm);
                handler.proceed(Client.INSTANCE.getBasicId(), Client.INSTANCE.getBasicPassword());
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if(request.isForMainFrame()) {
                    return shouldOverrideUrlLoading(view, request.getUrl().toString());
                }
                else{
                    return false;
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {

            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                //super.onReceivedError(view, request, error);
                if (request.isForMainFrame()) {
                    onReceivedError(view,
                            error.getErrorCode(), error.getDescription().toString(),
                            request.getUrl().toString());
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            }
        });

        mWebView.setBackgroundColor(0);
        mWebView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        mWebView.loadUrl( Client.INSTANCE.getEndpoint() + "menu-enquetes/");

    }

    public void setContact(Concierge.Contacts contact) {
        this.mContact = contact;
    }
}
