package jp.sunnygem.sumaiful.Tab.Concierge;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class InquiryFragment extends TabSubFragment {

    private Concierge.Contacts mContact = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_inquiry, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ImageButton back = view.findViewById(R.id.button_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.popFragment();
            }
        });


        Button callButton = view.findViewById(R.id.button_call);
        if(mContact != null) {
            String number = mContact.getTel();
            callButton.setText(number);
        }
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mContact == null) return;
                String number = mContact.getTel();
                Intent callIntent = new Intent(Intent.ACTION_CALL); //use ACTION_CALL class
                callIntent.setData(Uri.parse("tel:"+number));    //this is the phone number calling
                //check permission
                //If the device is running Android 6.0 (API level 23) and the app's targetSdkVersion is 23 or higher,
                //the system asks the user to grant approval.
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    //request permission from user if the app hasn't got the required permission
                    ActivityCompat.requestPermissions(mParent.getActivity(),
                            new String[]{Manifest.permission.CALL_PHONE},   //request specific permission from user
                            10);
                    return;
                }else {     //have got permission
                    try{
                        startActivity(callIntent);  //call activity and make phone call
                    }
                    catch (android.content.ActivityNotFoundException ex){
                    }
                }
            }
        });

        Button reserveButton = view.findViewById(R.id.button_reserve);

        String url = mContact.getReserveUrl();
        if(url.isEmpty()){
            reserveButton.setVisibility(View.GONE);
        }
        else{
            reserveButton.setVisibility(View.VISIBLE);
            String title = mContact.getReserveTitle();
            if(!title.isEmpty())
                reserveButton.setText(title);
        }
        reserveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mContact.getReserveUrl()));
                try{
                    startActivity(intent);
                }
                catch (android.content.ActivityNotFoundException ex){
                }

            }
        });

        Button webButton = view.findViewById(R.id.button_web);

        url = mContact.getUrl();
        if(url.isEmpty()){
            webButton.setVisibility(View.GONE);
        }
        else{
            webButton.setVisibility(View.VISIBLE);
            String title = mContact.getTitle();
            if(!title.isEmpty())
                webButton.setText(title);
        }


        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mContact.getUrl()));
                try{
                    startActivity(intent);
                }
                catch (android.content.ActivityNotFoundException ex){
                }
            }
        });


        TextView commentLabel = view.findViewById(R.id.label_comment);
        commentLabel.setText(mContact.getComment());


    }

    public void setContact(Concierge.Contacts contact) {
        this.mContact = contact;
    }
}
