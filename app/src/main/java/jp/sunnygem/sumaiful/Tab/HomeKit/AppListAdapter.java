package jp.sunnygem.sumaiful.Tab.HomeKit;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.Model.Application;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.URLImageView;
import kotlin.Unit;

public class AppListAdapter extends SelectViewAdapter<AppListAdapter.ViewHolder> {

    List<Application> mAppList = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_homekit_application, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.bind(mAppList.get(position));
    }

    @Override
    public int getItemCount() {
        return mAppList.size();
    }
    public Application getApplication(int idx){return mAppList.get(idx);}

    public void addApplication(Application app){
        mAppList.add(app);
        notifyDataSetChanged();
    }

    void clear(){
        mAppList.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView mTitle;
        URLImageView mIcon;
        View mFavoriteView;
        Application mApplication;
        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.label_title);
            mIcon = itemView.findViewById(R.id.view_icon);
            mFavoriteView = itemView.findViewById(R.id.view_favorite);
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(mApplication != null) {
                        mApplication.setFavorite(!mApplication.isFavorite());
                        updateFavorite();
                        Client.INSTANCE.saveFavorite(mApplication);
                    }
                    return true;
                }
            });
        }

        void updateFavorite(){
            if(mApplication.isFavorite()) mFavoriteView.setVisibility(View.VISIBLE);
            else  mFavoriteView.setVisibility(View.INVISIBLE);

        }

        public void bind(Application app){
            mApplication = app;
            mTitle.setText(app.getTitle());
            updateFavorite();
            Client.INSTANCE.fetchIconUrl(app.getAppId(), (url, error)->{
                Client.INSTANCE.getCurrentActivity().runOnUiThread(()->{
                    mIcon.loadUrl(url);
                });
                return null;
            });
        }
    }
}
