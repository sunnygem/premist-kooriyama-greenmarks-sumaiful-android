package jp.sunnygem.sumaiful.Tab;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.R;

public class TabBar extends LinearLayout implements TabBarItemView.ItemClickListener{
    public interface TabBarListener{
        void didClickTab(int idx);
    };

    private int mSelectedIndex = -1;
    private List<TabBarItemView> mTabList = new ArrayList<>();
    private TabBarListener mListener;

    public TabBar(Context context) {
        super(context);
    }

    public TabBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TabBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TabBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setListener(TabBarListener listener){
        mListener = listener;
    }

    public void clearItems(){
        removeAllViews();
        mTabList.clear();
    }

    public int getSelectedIndex(){
        return mSelectedIndex;
    }
    public TabBarItemView currentTabItem(){
        if(mSelectedIndex != -1) return mTabList.get(mSelectedIndex);
        return null;
    }
    public TabBarItemView tabItem(int index){
        return mTabList.get(index);
    }

    public TabBarItemView addItem(Drawable selectedImage, Drawable unselectedImage){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        TabBarItemView itemView = (TabBarItemView) inflater.inflate(R.layout.view_tab, null);

        itemView.setup(mTabList.size(), this);
        itemView.setImage(selectedImage, unselectedImage);

        LayoutParams layoutParams = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
        addView(itemView, layoutParams);
        if(mSelectedIndex == mTabList.size()){
            itemView.select(true);
        }

        mTabList.add(itemView);
        return itemView;
    }

    public int getCurrentSelectedPosition(){
        return mSelectedIndex;
    }

    public void selectTab(int idx, boolean flag){

        mSelectedIndex = idx;
        int count = mTabList.size();
        for(int i = 0; i<count; ++i){
            mTabList.get(i).select(i == idx);
        }

    }

    @Override
    public void didClickTab(int index) {
        selectTab(index, true);
        if(mListener != null){
            mListener.didClickTab(index);
        }
    }
}
