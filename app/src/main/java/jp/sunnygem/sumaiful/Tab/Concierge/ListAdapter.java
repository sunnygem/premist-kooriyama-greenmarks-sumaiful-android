package jp.sunnygem.sumaiful.Tab.Concierge;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.URLImageView;

public class ListAdapter extends SelectViewAdapter<ListAdapter.ViewHolder> {

    List<Concierge> mConcierges = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_concierge, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mConcierges.get(position));
        super.onBindViewHolder(holder, position);

    }

    Concierge getConcierge(int idx){return mConcierges.get(idx);}

    @Override
    public int getItemCount() {
        return mConcierges.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTitleLabel;
        TextView mSubtitleLabel;
        URLImageView mIconView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleLabel = itemView.findViewById(R.id.label_title);
            mSubtitleLabel = itemView.findViewById(R.id.label_subtitle);
            mIconView = itemView.findViewById(R.id.view_icon);
        }


        public void bind(Concierge data){
            if(mTitleLabel != null) mTitleLabel.setText(data.getName());
            if(mSubtitleLabel != null) mSubtitleLabel.setText(data.getInformation());
            if(mIconView != null) mIconView.loadUrl(data.getIconUrl());
        }
    }

    public void addConcierge(Concierge data){mConcierges.add(data); notifyDataSetChanged();}
    void clear(){mConcierges.clear(); notifyDataSetChanged();}

}
