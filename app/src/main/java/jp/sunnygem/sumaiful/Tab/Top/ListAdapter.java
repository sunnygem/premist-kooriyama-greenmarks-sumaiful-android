package jp.sunnygem.sumaiful.Tab.Top;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.Model.Information;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.URLImageView;


public class ListAdapter extends SelectViewAdapter<ListAdapter.ViewHolder> {

    List<Information> mDataList = new ArrayList<>();
    private Context mContext;

    public ListAdapter(Context context){
        mContext = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_information, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectListener != null){
                    mSelectListener.OnSelect(position);
                }
            }
        });
        holder.bind(mDataList.get(position));
    }

    public Information getInformation(int index){
        return mDataList.get(index);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mDateLabel;
        TextView mTitleLabel;
        URLImageView mIconView;
        ImageView mIconTypeView;

        public ViewHolder(View itemView) {
            super(itemView);
            mDateLabel = itemView.findViewById(R.id.label_date);
            mIconView = itemView.findViewById(R.id.view_icon);
            mIconTypeView = itemView.findViewById(R.id.view_icon_type);
            mTitleLabel = itemView.findViewById(R.id.label_title);
        }

        public void bind(Information info){



            mDateLabel.setText(Client.INSTANCE.getPresentDateFormat().format(info.getDate()));
            mTitleLabel.setText(info.getTitle());
            if(info.getLink() != null){
                mIconTypeView.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_link));
            }
            else{
                mIconTypeView.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.icon_pdf));
            }
            /*
            List<Information.Image> images = info.getImages();
            if(!images.isEmpty()){
                mIconView.loadUrl(images.get(0).getUrl());
            }
            */
        }
    }

    public void addInformation(Information info){
        mDataList.add(info);
        notifyItemInserted(mDataList.size()-1);
    }
    public void clear(){
        int cnt = mDataList.size();
        mDataList.clear();
        notifyItemRangeRemoved(0, cnt);
    }
}