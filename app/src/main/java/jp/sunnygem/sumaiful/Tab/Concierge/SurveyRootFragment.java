package jp.sunnygem.sumaiful.Tab.Concierge;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Model.Survey;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class SurveyRootFragment extends TabSubFragment {
    protected Survey mSurvey;

    public Survey getSurvey() {
        return mSurvey;
    }

    public void setSurvey(Survey survey) {
        mSurvey = survey;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mSurvey == null) return;

        TextView dateLabel = view.findViewById(R.id.label_date);
        dateLabel.setText(mSurvey.getDate());
        TextView titleLabel = view.findViewById(R.id.label_title);
        titleLabel.setText(mSurvey.getTitle());

        TextView numberLabel = view.findViewById(R.id.label_question_no);
        EditText contentsLabel = view.findViewById(R.id.label_contents);


        Button okButton = view.findViewById(R.id.button_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                okButtonPressed();
            }
        });

    }

    public void okButtonPressed(){
        mParent.popFragment();
    }

    @Override
    public boolean usesHeader() {
        return false;
    }
}
