package jp.sunnygem.sumaiful.Tab.Top

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import java.io.File

import jp.sunnygem.sumaiful.Tab.TabSubFragment
import jp.wasabeef.recyclerview.animators.FadeInAnimator
import jp.sunnygem.sumaiful.Client
import jp.sunnygem.sumaiful.IndicatedRecyclerView
import jp.sunnygem.sumaiful.Model.Accessory
import jp.sunnygem.sumaiful.Model.Information
import jp.sunnygem.sumaiful.PdfViewActivity
import jp.sunnygem.sumaiful.ProgressDialog
import jp.sunnygem.sumaiful.R
import jp.sunnygem.sumaiful.SelectViewAdapter
import jp.sunnygem.sumaiful.Tab.Top.ListAdapter

import android.support.v7.widget.RecyclerView.HORIZONTAL

class MainFragment : TabSubFragment() {

    var mInfoAdapter: ListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_fragment_top_main, container, false)
    }

    fun reload(callback:(()->Unit)? = null){
        val view1 = view
        val infoList = view1!!.findViewById<IndicatedRecyclerView>(R.id.list_information)

        infoList.setLoading(true)
        Client.fetchTopInformation { information, e ->
            val activity = activity
            activity?.runOnUiThread {
                mInfoAdapter?.clear()
                for (info in information) {
                    mInfoAdapter?.addInformation(info)
                }
                infoList.setLoading(false)
                callback?.invoke()
            }
            null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        mInfoAdapter = ListAdapter(this.context)

        mInfoAdapter?.setOnSelectListener { index ->
            val information = mInfoAdapter?.getInformation(index)

            val fragment = ContentFragment()
            fragment.setInformation(information)
            mParent.pushFragment(fragment)
        }


        val view1 = getView()

        val refresh = view.findViewById<SwipeRefreshLayout>(R.id.layout_refresh)
        refresh.setOnRefreshListener {
            reload{
                refresh.isRefreshing = false
            }
        }
        val infoList = view1!!.findViewById<IndicatedRecyclerView>(R.id.list_information)
        infoList.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        infoList.adapter = mInfoAdapter
        infoList.itemAnimator = FadeInAnimator()

        reload()

    }
}
