package jp.sunnygem.sumaiful.Tab.Sumaiful

import android.app.Activity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import jp.wasabeef.recyclerview.animators.FadeInAnimator
import jp.sunnygem.sumaiful.Client
import jp.sunnygem.sumaiful.IndicatedRecyclerView
import jp.sunnygem.sumaiful.Model.MoreSumaiful
import jp.sunnygem.sumaiful.R
import jp.sunnygem.sumaiful.SelectViewAdapter
import jp.sunnygem.sumaiful.Tab.TabSubFragment

class MainFragment : TabSubFragment() {


    var mAdapter: ListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_fragment_sumaiful_main, container, false)
    }

    fun reload(callback:(()->Unit)? = null){
        val view1 = view
        val list = view1!!.findViewById<IndicatedRecyclerView>(R.id.list)

        list.setLoading(true)

        Client.fetchMoreSumaiful { moreSumaifuls, e ->
            val activity = activity
            activity?.runOnUiThread {
                mAdapter?.clear()
                for (i in moreSumaifuls) {
                    mAdapter?.addInformation(i)
                }
                list.setLoading(false)
                callback?.invoke()
            }
            null
        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = ListAdapter(context)

        mAdapter?.setOnSelectListener { index ->
            mAdapter?.getInformation(index)?.let{
                openInformation(it)
            }
        }

        val refresh = view.findViewById<SwipeRefreshLayout>(R.id.layout_refresh)
        refresh.setOnRefreshListener {
            reload {
                refresh.isRefreshing = false
            }
        }

        val view1 = getView()
        val list = view1!!.findViewById<RecyclerView>(R.id.list)
        list.adapter = mAdapter
        list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        list.itemAnimator = FadeInAnimator()

        reload()

    }

    internal fun openInformation(info: MoreSumaiful) {
        val contentFragment = ContentFragment()
        contentFragment.setInformation(info)
        mParent.pushFragment(contentFragment)
    }
}
