package jp.sunnygem.sumaiful.Tab.HomeKit;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import jp.sunnygem.sumaiful.URLImageView;
import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.sunnygem.sumaiful.Model.Accessory;
import jp.sunnygem.sumaiful.Model.Application;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class AppListFragment extends TabSubFragment {

    AppListAdapter mAdapter = new AppListAdapter();
    Accessory mAccessory;
    URLImageView mIconView;
    TextView mMemoLabel;
    String mMemo = "";




    void setAccessory(Accessory a){
        mAccessory = a;
        mMemo = a.getMemo();
        for(Application app : a.getAppList()){
            mAdapter.addApplication(app);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_homekit_application, container, false);
    }

    public void setMemo(String memo) {
        this.mMemo = memo;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView list = view.findViewById(R.id.list);

        mIconView = view.findViewById(R.id.icon);

        mMemoLabel = view.findViewById(R.id.label_memo);
        mMemoLabel.setText(mMemo);

        mIconView.loadUrl(mAccessory.getIconUrl());
        mAdapter.setOnSelectListener(new SelectViewAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                mAdapter.getApplication(index);

            }
        });

        list.setAdapter(mAdapter);
        list.setLayoutManager(new GridLayoutManager(getContext(), 3));
        list.setItemAnimator(new FadeInAnimator());

        mAdapter.setOnSelectListener(new SelectViewAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                Application application = mAdapter.getApplication(index);
                String scheme = application.getScheme();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(scheme));
                boolean failed = false;
                try {
                    startActivity(intent);
                }
                catch(ActivityNotFoundException exception){

                    failed = true;
                }

                if(failed) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(application.getStoreUrl()));
                    try {
                        startActivity(intent);
                    }
                    catch(ActivityNotFoundException exception){

                        new AlertDialog.Builder(getActivity())
                                .setTitle(application.getTitle())
                                .setMessage(getResources().getString(R.string.error_app_not_found))
                                .setPositiveButton("OK", null)
                                .show();

                    }
                }



            }
        });

        TextView title = view.findViewById(R.id.label_title);
        title.setText(mAccessory.getTitle());

        ImageButton back = view.findViewById(R.id.button_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.popFragment();
            }
        });

    }
}
