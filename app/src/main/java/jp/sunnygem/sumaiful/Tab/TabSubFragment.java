package jp.sunnygem.sumaiful.Tab;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.PdfViewActivity;
import jp.sunnygem.sumaiful.ProgressDialog;

public class TabSubFragment extends Fragment {
    protected TabRootFragment mParent = null;
    protected boolean mUsesHeader = true;

    void setParent(TabRootFragment parent){
        mParent = parent;
    }
    public boolean usesHeader(){return mUsesHeader;}

    public void willAppear(){}

    public void openPdf(String link) {
        ProgressDialog dialog = ProgressDialog.newInstance("Loading...");
        dialog.show(getFragmentManager(), "");

        Client.INSTANCE.download(link, (file, e) -> {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.cancel();
                    if(file != null){

                        Intent next = new Intent(getContext(), PdfViewActivity.class);
                        next.putExtra("URL", file.getPath());
                        startActivity(next);
                    }
                }
            });
            return null;
        });
    }

    public void openWebURL(String link){
        if(link != null && !link.isEmpty()){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(browserIntent);
        }
    }

    public void openURL(String link){
        if(link != null && !link.isEmpty()){
            if(link.endsWith("pdf")) openPdf(link);
            else openWebURL(link);
        }
    }

}
