package jp.sunnygem.sumaiful.Tab.Top;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabRootFragment;

public class TopFragment extends TabRootFragment {

    MainFragment mMainFragment = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_top, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void reset(){
        reset(new MainFragment());
    }


}