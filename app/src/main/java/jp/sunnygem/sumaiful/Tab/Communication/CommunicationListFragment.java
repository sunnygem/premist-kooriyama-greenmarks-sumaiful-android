package jp.sunnygem.sumaiful.Tab.Communication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.IndicatedRecyclerView;
import jp.sunnygem.sumaiful.Model.Communication;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class CommunicationListFragment extends TabSubFragment {


    CommunicationListAdapter mAdapter = new CommunicationListAdapter();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_communication_main, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View root = getView();

        RecyclerView list = root.findViewById(R.id.list);

        mAdapter.setOnSelectListener(new ThreadListAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                openCommunication(mAdapter.getCommunications().get(index));
            }
        });

        list.setItemAnimator(new FadeInAnimator());
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(mAdapter);

        reload();
    }


    private void reload(){
        View view1 = getView();
        final IndicatedRecyclerView listView = view1.findViewById(R.id.list);

    }

    void openCommunication(Communication com){
        ThreadListFragment fragment = new ThreadListFragment();
        fragment.setCommunication(com);
        mParent.pushFragment(fragment);
    }
}
