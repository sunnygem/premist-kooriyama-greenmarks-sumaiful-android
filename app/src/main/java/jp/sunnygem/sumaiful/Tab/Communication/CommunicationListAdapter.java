package jp.sunnygem.sumaiful.Tab.Communication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Model.Communication;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;

public class CommunicationListAdapter extends SelectViewAdapter<CommunicationListAdapter.ViewHolder> {

    private List<Communication> mCommunications = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_communication, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mCommunications.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectListener != null) {
                    mSelectListener.OnSelect(position);
                }
            }
        });
        holder.getButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectListener != null) {
                    mSelectListener.OnSelect(position);
                }
            }
        });
    }

    public void addCommunication(Communication com){
        mCommunications.add(com);
        notifyItemInserted(mCommunications.size()-1);
    }
    public void clearCommunications(){
        int cnt = mCommunications.size();
        mCommunications.clear();
        notifyItemRangeRemoved(0, cnt);
    }

    public List<Communication> getCommunications() {
        return mCommunications;
    }

    @Override
    public int getItemCount() {
        return mCommunications.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        Button mButton;

        public ViewHolder(View itemView) {
            super(itemView);
            mButton = itemView.findViewById(R.id.button_communication);
        }

        public void bind(Communication data){
            mButton.setText(data.getTitle());
        }

        public Button getButton() {
            return mButton;
        }
    }
}
