package jp.sunnygem.sumaiful.Tab.Concierge;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.Model.Specs;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;

public class SpecsListAdapter extends SelectViewAdapter<SpecsListAdapter.ViewHolder> {

    List<Concierge.Manual> mManuals = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_specs, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public int getItemCount()
    {
        if(mManuals == null) return 0;
        return mManuals.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        holder.bind(mManuals.get(position));
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitleLabel;
        private TextView mDateLabel;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleLabel = itemView.findViewById(R.id.label_title);
            mDateLabel = itemView.findViewById(R.id.label_date);
        }


        public void bind(Concierge.Manual data){
            mTitleLabel.setText(data.getName());
            mDateLabel.setText(Client.INSTANCE.getPresentDateFormat().format(data.getDate()) + " UP");
        }
    }


    Concierge.Manual manual(int idx){return mManuals.get(idx);};
    public void addManual(Concierge.Manual data){mManuals.add(data); notifyDataSetChanged();}
    void clear(){mManuals.clear(); notifyDataSetChanged();}

}
