package jp.sunnygem.sumaiful.Tab.Communication;

import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabRootFragment;

public class CommunicationFragment extends TabRootFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_communication, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void reset(){
        reset(new CommunicationListFragment());
    }

}
