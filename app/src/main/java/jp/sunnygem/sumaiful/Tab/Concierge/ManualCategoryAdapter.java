package jp.sunnygem.sumaiful.Tab.Concierge;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.Model.ManualCategory;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.URLImageView;

public class ManualCategoryAdapter extends SelectViewAdapter<ManualCategoryAdapter.ViewHolder> {

    List<ManualCategory> mManualCategories = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_manual_category, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mManualCategories.get(position));
        super.onBindViewHolder(holder, position);

    }

    ManualCategory getCategory(int idx){return mManualCategories.get(idx);}

    @Override
    public int getItemCount() {
        return mManualCategories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTitleLabel;
        TextView mSubtitleLabel;
        URLImageView mIconView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleLabel = itemView.findViewById(R.id.label_title);
            mSubtitleLabel = itemView.findViewById(R.id.label_subtitle);
            mIconView = itemView.findViewById(R.id.view_icon);
        }


        public void bind(ManualCategory data){
            if(mTitleLabel != null) mTitleLabel.setText(data.getTitle());
            if(mSubtitleLabel != null) mSubtitleLabel.setText("");
            if(mIconView != null) mIconView.loadUrl(data.getUrl());
        }
    }

    public void addCategory(ManualCategory data){mManualCategories.add(data); notifyDataSetChanged();}
    void clear(){mManualCategories.clear(); notifyDataSetChanged();}

}
