package jp.sunnygem.sumaiful.Tab.Top;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.ImageViewer.ImageViewerActivity;
import jp.sunnygem.sumaiful.Model.Information;
import jp.sunnygem.sumaiful.Model.MoreSumaiful;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;
import jp.sunnygem.sumaiful.URLImageView2;

import java.util.List;

public class ContentFragment extends TabSubFragment {

    Information mInfo;

    void setInformation(Information i){
        mInfo = i;
    }

    @Override
    public boolean usesHeader() {
        return false;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_top_content, container, false);

    }

    void open(){
        String link = mInfo.getLink();
        if(link != null && !link.isEmpty()){
            openURL(link);
        }
        else{
            List<Information.Pdf> pdfs = mInfo.getPdfs();
            if(!pdfs.isEmpty()) {
                openPdf(pdfs.get(0).getUrl());
            }

        }
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageButton back = view.findViewById(R.id.button_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.popFragment();
            }
        });

        TextView date = view.findViewById(R.id.label_date);
        date.setText(Client.INSTANCE.getPresentDateFormat().format(mInfo.getDate()));

        TextView title = view.findViewById(R.id.label_title);
        title.setText(mInfo.getTitle());

        TextView contents = view.findViewById(R.id.label_content);
        contents.setText(mInfo.getContent());


        URLImageView2 contentImageView = view.findViewById(R.id.image_content);

        contentImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ImageViewerActivity.class);

                intent.putExtra("url", mInfo.getImages().get(0).getUrl());
                startActivity(intent);
//                startActivity(intent, ActivityOptions.makeCustomAnimation(activity, R.anim.fade_in, R.anim.fade_out).toBundle())


            }
        });
        List<Information.Image> images = mInfo.getImages();
        if(!images.isEmpty()){
            contentImageView.loadUrl(images.get(0).getUrl(), false, false);
        }

        TextView linkView = view.findViewById(R.id.label_link);
        URLImageView2 imageView = view.findViewById(R.id.image_pdf);



        String link = mInfo.getLink();
        if(link != null && !link.isEmpty()) {
            SpannableString content = new SpannableString(link);
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            linkView.setText(content);
            imageView.setVisibility(View.GONE);
            linkView.setOnClickListener(v -> getActivity().runOnUiThread(() -> open()));
        }
        else{
            List<Information.Pdf> pdfs = mInfo.getPdfs();
            if(!pdfs.isEmpty()) {
                imageView.loadUrl(pdfs.get(0).getUrl(), true, false);
            }

            linkView.setVisibility(View.GONE);

            imageView.setOnClickListener(v -> getActivity().runOnUiThread(() -> open()));


        }

    }
}
