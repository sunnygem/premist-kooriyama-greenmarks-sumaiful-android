package jp.sunnygem.sumaiful.Tab.Sumaiful;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.*;
import jp.sunnygem.sumaiful.Model.MoreSumaiful;

public class ListAdapter extends SelectViewAdapter<ListAdapter.ViewHolder> {

    List<MoreSumaiful> mDataList = new ArrayList<>();
    private Context mContext;

    public ListAdapter(Context context){
        mContext = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_more_sumaiful, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectListener != null){
                    mSelectListener.OnSelect(position);
                }
            }
        });
        holder.bind(mDataList.get(position));
    }

    public MoreSumaiful getInformation(int index){
        return mDataList.get(index);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mDateLabel;
        TextView mTitleLabel;
        HeightBasedAspectRatioImageView mIconView;

        public ViewHolder(View itemView) {
            super(itemView);
            mDateLabel = itemView.findViewById(R.id.label_date);
            mIconView = itemView.findViewById(R.id.view_icon);
            mTitleLabel = itemView.findViewById(R.id.label_title);
        }

        public void bind(MoreSumaiful info){
            mDateLabel.setText(Client.INSTANCE.getPresentDateFormat().format(info.getDate()));
            mTitleLabel.setText(info.getTitle());

            List<MoreSumaiful.Image> images = info.getImages();
            if(!images.isEmpty()){
                mIconView.loadUrl(images.get(0).getUrl(), true, false);
            }
        }
    }

    public void addInformation(MoreSumaiful info){
        mDataList.add(info);
        notifyItemInserted(mDataList.size()-1);
    }
    public void clear(){
        int cnt = mDataList.size();
        mDataList.clear();
        notifyItemRangeRemoved(0, cnt);
    }
}