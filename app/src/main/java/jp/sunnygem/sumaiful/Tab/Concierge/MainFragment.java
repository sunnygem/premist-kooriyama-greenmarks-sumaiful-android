package jp.sunnygem.sumaiful.Tab.Concierge;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.IndicatedRecyclerView;
import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class MainFragment extends TabSubFragment {

    ListAdapter mAdapter = new ListAdapter();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_main, container, false);
    }


    private void reload(){
        View view1 = getView();
        final IndicatedRecyclerView listView = view1.findViewById(R.id.list_concierge);

        listView.setLoading(true);
        Client.INSTANCE.fetchAllConcierges((concierges, e) -> {
            Activity activity = getActivity();
            if(activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.clear();
                        for (Concierge c : concierges) {
                            mAdapter.addConcierge(c);
                        }
                        listView.setLoading(false);
                    }
                });
            }
            return null;
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View view1 = getView();
        RecyclerView list = view1.findViewById(R.id.list_concierge);

        mAdapter.setOnSelectListener(new jp.sunnygem.sumaiful.Tab.HomeKit.ListAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                openConcierge(mAdapter.getConcierge(index));
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        list.setLayoutManager(gridLayoutManager);
        list.setAdapter(mAdapter);
        list.setItemAnimator(new FadeInAnimator());


        reload();
    }

    void openConcierge(Concierge a){

        int type = a.getType();
        Fragment nextFragment = null;
        switch (type){
            case 1: //ContactPhone
                InquiryFragment inquiry = new InquiryFragment();
                inquiry.setContact((Concierge.Contacts) a.getData());
                nextFragment = inquiry;
                break;

            case 6: //Applications
                AppListFragment appListFragment = new AppListFragment();
                Concierge.Applications apps = (Concierge.Applications) a.getData();
                apps.setIconUrl(a.getIconUrl());
                appListFragment.setMemo(apps.getMemo());
                appListFragment.setApps(apps);
                nextFragment = appListFragment;
                break;

            case 7: //Manual
                ManualCategoryFragment specs = new ManualCategoryFragment();
                nextFragment = specs;
                break;

            case 9: //Survey
                SurveyWebFragment survey = new SurveyWebFragment();
                nextFragment = survey;
                break;

            case 11: //Link
                Concierge.Links links = (Concierge.Links) a.getData();
                ArrayList<String> links1 = links.getLinks();
                if(!links1.isEmpty()){
                    openURL(links1.get(0));
                }
                break;
        }

        if(nextFragment != null) {
            mParent.pushFragment(nextFragment);
        }
    }


}
