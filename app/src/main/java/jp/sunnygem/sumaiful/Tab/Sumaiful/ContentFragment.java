package jp.sunnygem.sumaiful.Tab.Sumaiful;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.Model.MoreSumaiful;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class ContentFragment extends TabSubFragment {

    MoreSumaiful mInfo;

    void setInformation(MoreSumaiful i){
        mInfo = i;
    }

    @Override
    public boolean usesHeader() {
        return false;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_sumaiful_content, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageButton back = view.findViewById(R.id.button_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.popFragment();
            }
        });

        TextView date = view.findViewById(R.id.label_date);
        date.setText(Client.INSTANCE.getPresentDateFormat().format(mInfo.getDate()));

        TextView title = view.findViewById(R.id.label_title);
        title.setText(mInfo.getTitle());

        TextView contents = view.findViewById(R.id.label_contents);
//        contents.setText();

        WebView webView = view.findViewById(R.id.view_contents);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(0);
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        webView.loadDataWithBaseURL(null, mInfo.getContents(),  "text/html", "utf-8", null);

    }
}
