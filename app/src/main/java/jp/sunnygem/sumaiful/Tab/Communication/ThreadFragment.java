package jp.sunnygem.sumaiful.Tab.Communication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.IndicatedRecyclerView;
import jp.sunnygem.sumaiful.MainActivity;
import jp.sunnygem.sumaiful.Model.Communication;
import jp.sunnygem.sumaiful.OnSwipeTouchListener;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class ThreadFragment extends TabSubFragment {


    ThreadAdapter mAdapter = new ThreadAdapter();
    Communication.Thread mThread = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_communication_talk, container, false);
    }

    @Override
    public boolean usesHeader() {
        return false;
    }

    void setThread(Communication.Thread thread){
        mThread = thread;
    }

    void reload(){
        View view1 = getView();
        final IndicatedRecyclerView listView = view1.findViewById(R.id.list);
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity activity = (MainActivity) getActivity();
        activity.setPagingEnabled(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        MainActivity activity = (MainActivity) getActivity();
        activity.setPagingEnabled(true);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View root = getView();

        TextView title = root.findViewById(R.id.label_title_header);
        TextView type = root.findViewById(R.id.label_type_header);
        title.setText(mThread.getTitle());
        type.setText(mThread.getCommunication().getTitle());

        view.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            public void onSwipeRight() {
                mParent.popFragment();
            }
        });


        EditText input = root.findViewById(R.id.field_input);
        ImageButton button = root.findViewById(R.id.button_enter);

        input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    button.setImageDrawable(getResources().getDrawable(R.mipmap.communication_msg_send));
                }
                else{
                    button.setImageDrawable(getResources().getDrawable(R.mipmap.communication_msg_camera));
                }
            }
        });

        RecyclerView list = root.findViewById(R.id.list);
        mAdapter.setThread(mThread);
        list.setItemAnimator(new FadeInAnimator());
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(mAdapter);

        list.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            public void onSwipeRight() {

                mParent.popFragment();
            }

        });

        reload();
    }

}
