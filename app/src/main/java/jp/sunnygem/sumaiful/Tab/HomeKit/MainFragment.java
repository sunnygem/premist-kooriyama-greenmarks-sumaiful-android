package jp.sunnygem.sumaiful.Tab.HomeKit;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import jp.sunnygem.sumaiful.*;
import jp.sunnygem.sumaiful.Modal.UnsupportedFragment;
import jp.sunnygem.sumaiful.Model.Accessory;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;

public class MainFragment extends TabSubFragment {


    ListAdapter mAdapter = new ListAdapter();
    UnsupportedFragment mWarningFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_homekit_main, container, false);
    }

    private void reload(){
        if(Client.INSTANCE.getMyUser().getType() != 1) return;

        View view1 = getView();
        final IndicatedRecyclerView listView = view1.findViewById(R.id.list);

        listView.setLoading(true);
        Client.INSTANCE.fetchAllAccessories((accessories, e) -> {
            Activity activity = getActivity();
            if(activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.clear();
                        for (Accessory a : accessories) {
                            mAdapter.addAccessory(a);
                        }
                        listView.setLoading(false);
                    }
                });
            }
            return null;
        });
    }


    @Override
    public void willAppear() {
        if(Client.INSTANCE.getMyUser().getType() != 1){
            showUnsupportedMessage();
        }
    }

    void showUnsupportedMessage(){
        if(mWarningFragment != null) return;

        UnsupportedFragment fragment = new UnsupportedFragment();
        mWarningFragment = fragment;
        fragment.setDidTouchCallback(new Runnable() {
            @Override
            public void run() {}
        });

        FragmentManager fragmentManager = this.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.view_warning, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mAdapter.setOnSelectListener(new ListAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {
                openAccessory(mAdapter.getAccessory(index));
            }
        });


        View view1 = getView();
        RecyclerView list = view1.findViewById(R.id.list);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        list.setLayoutManager(gridLayoutManager);
        list.setAdapter(mAdapter);


        reload();
    }

    void openAccessory(Accessory a){
        AppListFragment appListFragment = new AppListFragment();
        appListFragment.setAccessory(a);
        mParent.pushFragment(appListFragment);
    }

}
