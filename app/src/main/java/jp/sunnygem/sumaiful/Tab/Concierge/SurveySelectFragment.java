package jp.sunnygem.sumaiful.Tab.Concierge;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.sunnygem.sumaiful.Model.SelectSurvey;
import jp.sunnygem.sumaiful.R;

public class SurveySelectFragment extends SurveyRootFragment {

    SurveySelectAdapter mAdapter = new SurveySelectAdapter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_survey_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter.setContext(getContext());
        mAdapter.setSurvey((SelectSurvey) mSurvey);
        RecyclerView listView = view.findViewById(R.id.list_selection);
        listView.setAdapter(mAdapter);
        listView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void okButtonPressed() {
        super.okButtonPressed();
    }
}
