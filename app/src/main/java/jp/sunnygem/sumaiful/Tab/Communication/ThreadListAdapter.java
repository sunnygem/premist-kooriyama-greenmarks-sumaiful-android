package jp.sunnygem.sumaiful.Tab.Communication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Model.Communication;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;

public class ThreadListAdapter extends SelectViewAdapter<ThreadListAdapter.ViewHolder> {

    private Communication mCommunication;
    private boolean mIsReloading = false;
    private int mLastDataSize = 0;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_communication_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mCommunication.getThreads().get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectListener != null) {
                    mSelectListener.OnSelect(position);
                }
            }
        });
    }

    public Communication getCommunication() {
        return mCommunication;
    }

    public void setCommunication(Communication communication) {
        mCommunication = communication;
    }

    public void willReloadData(){
        mLastDataSize = mCommunication.getThreads().size();
    }

    public void reloadData(){
        int size = mCommunication.getThreads().size();
        if(mLastDataSize > 0) {
            notifyDataSetChanged();
            //Cause crash..
            /*
            mIsReloading = true;
            notifyItemRangeRemoved(0, mLastDataSize - 1);
            mIsReloading = false;
            */
        }
        else
            notifyItemRangeInserted(0, size);
    }

    @Override
    public int getItemCount() {
        if(mIsReloading) return 0;
        return mCommunication.getThreads().size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView mIconView;
        TextView mContentLabel;
        TextView mDateLabel;
        TextView mUserNameLabel;
        TextView mTypeLabel;

        public ViewHolder(View itemView) {
            super(itemView);
            mIconView = itemView.findViewById(R.id.image_icon);
            mContentLabel = itemView.findViewById(R.id.label_title);
            mDateLabel = itemView.findViewById(R.id.label_date);
            mUserNameLabel = itemView.findViewById(R.id.label_username);
            mTypeLabel = itemView.findViewById(R.id.label_type_header);
        }

        public void bind(Communication.Thread data){

//            mIconView. data.getUser().getIconUrl()
            mContentLabel.setText(data.getTitle());
            mDateLabel.setText(data.getDate());
            mUserNameLabel.setText(data.getUser().getName());
        }
    }
}
