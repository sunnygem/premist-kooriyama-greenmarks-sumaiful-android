package jp.sunnygem.sumaiful.Tab.HomeKit;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Model.Accessory;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.URLImageView;

public class ListAdapter extends SelectViewAdapter<ListAdapter.ViewHolder> {

    List<Accessory> mAccessories = new ArrayList<>();
    boolean mIsLastUsed = false;

    public ListAdapter() {
    }
    public ListAdapter(boolean isLastUsed){
        mIsLastUsed = isLastUsed;
    }

    Accessory getAccessory(int index){
        return mAccessories.get(index);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(mIsLastUsed ? R.layout.view_item_homekit_last_used : R.layout.view_item_homekit, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mAccessories.get(position));
        super.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return mAccessories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTitleLabel;
        TextView mSubtitleLabel;
        URLImageView mIconView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleLabel = itemView.findViewById(R.id.label_title);
            mSubtitleLabel = itemView.findViewById(R.id.label_subtitle);
            mIconView = itemView.findViewById(R.id.view_icon);
        }


        public void bind(Accessory data){
            if(mTitleLabel != null) mTitleLabel.setText(data.getTitle());
            if(mSubtitleLabel != null) mSubtitleLabel.setText(data.getSubtitle());
            if(mIconView != null) mIconView.loadUrl(data.getIconUrl());
        }
    }

    public void addAccessory(Accessory data){mAccessories.add(data); notifyDataSetChanged();}
    void clear(){mAccessories.clear(); notifyDataSetChanged();}
}
