package jp.sunnygem.sumaiful.Tab.Concierge;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Model.Application;
import jp.sunnygem.sumaiful.Model.Concierge;
import jp.sunnygem.sumaiful.R;
import jp.sunnygem.sumaiful.SelectViewAdapter;
import jp.sunnygem.sumaiful.Tab.HomeKit.AppListAdapter;
import jp.sunnygem.sumaiful.Tab.TabSubFragment;
import jp.sunnygem.sumaiful.URLImageView;

public class AppListFragment extends TabSubFragment {

    Concierge.Applications mApps;
    AppListAdapter mAdapter = new AppListAdapter();
    URLImageView mIconView;
    TextView mMemoLabel;


    String mMemo = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_concierge_application, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mMemoLabel = view.findViewById(R.id.label_memo);
        mMemoLabel.setText(mMemo);

        RecyclerView list = view.findViewById(R.id.list);
        mIconView = view.findViewById(R.id.icon);

        mIconView.loadUrl(mApps.getIconUrl());
        mAdapter.setOnSelectListener(new SelectViewAdapter.OnSelectListener() {
            @Override
            public void OnSelect(int index) {

                mAdapter.getApplication(index);

            }
        });

        list.setAdapter(mAdapter);
        list.setLayoutManager(new GridLayoutManager(getContext(), 3));
        for(Application app : mApps.getApps()){
            mAdapter.addApplication(app);
        }


        TextView title = view.findViewById(R.id.label_title);
        title.setText(mApps.getName());

        ImageButton back = view.findViewById(R.id.button_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.popFragment();
            }
        });

    }
    public void setMemo(String memo) {
        this.mMemo = memo;
    }

    public void setApps(Concierge.Applications apps) {
        this.mApps = apps;
    }
}
