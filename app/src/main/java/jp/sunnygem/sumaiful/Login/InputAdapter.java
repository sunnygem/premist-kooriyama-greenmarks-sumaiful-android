package jp.sunnygem.sumaiful.Login;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.R;

public class InputAdapter extends RecyclerView.Adapter<InputAdapter.ViewHolder> {

    public static class Input{
        String mTitle;
        String mValue;
        boolean isSecured = false;


        public Input(String title){
            mTitle = title;
        }
        public Input(String title, boolean secured){
            mTitle = title;
            isSecured = secured;
        }
        public Input(String title, String value, boolean secured){
            mTitle = title;
            mValue = value;
            isSecured = secured;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(String title) {
            mTitle = title;
        }

        public String getValue() {
            return mValue;
        }

        public void setValue(String value) {
            mValue = value;
        }
    }

    List<Input> mList = new ArrayList<>();

    @NonNull
    @Override
    public InputAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_login_input, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InputAdapter.ViewHolder holder, int position) {
        holder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public void clearItems(){
        mList.clear();
        notifyDataSetChanged();
    }
    public void addItem(Input input){
        mList.add(input);
        notifyDataSetChanged();
    }



    class ViewHolder extends RecyclerView.ViewHolder{

        TextView mTitleLabel;
        EditText mInput;
        Input mData;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleLabel = itemView.findViewById(R.id.label_title);
            mInput = itemView.findViewById(R.id.input_value);
            mInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(mData != null){
                        mData.setValue(charSequence.toString());
                    }

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        public void bind(Input input){
            mData = input;
            mTitleLabel.setText(input.getTitle());
            mInput.setText(input.getValue());
        }
    };

}
