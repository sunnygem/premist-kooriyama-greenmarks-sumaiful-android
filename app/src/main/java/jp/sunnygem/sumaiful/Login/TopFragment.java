package jp.sunnygem.sumaiful.Login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import jp.sunnygem.sumaiful.R;

public class TopFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_login_top, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button newUser = view.findViewById(R.id.button_new_user);

        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getParentActivity().loadFragment("NewUser");
            }
        });

        Button signIn = view.findViewById(R.id.button_sign_in);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getParentActivity().loadFragment("SignIn");
            }
        });

        Button top = view.findViewById(R.id.button_top_limited);

        top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getParentActivity().moveToMain();
            }
        });

        Button family = view.findViewById(R.id.button_family_new_user);

        family.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpFragment signup = (SignUpFragment)getParentActivity().loadFragment("SignUp");
                signup.setIsFamilyMode(true);
            }
        });


    }
}
