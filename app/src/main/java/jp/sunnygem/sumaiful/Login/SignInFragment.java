package jp.sunnygem.sumaiful.Login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.LoginActivity;
import jp.sunnygem.sumaiful.ProgressDialog;
import jp.sunnygem.sumaiful.R;

public class SignInFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_login_sign_in, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EditText id = view.findViewById(R.id.field_id);
        EditText password = view.findViewById(R.id.field_password);

        Button loginButton = view.findViewById(R.id.button_sign_in);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(id.getText().toString().isEmpty() || password.getText().toString().isEmpty()){
                    return;
                }

                ProgressDialog dialog = ProgressDialog.newInstance("Logging in...");
                dialog.show(getFragmentManager(), "");

                Client.INSTANCE.login(id.getText().toString(), password.getText().toString(), (user, e)->{
                    LoginActivity parentActivity = getParentActivity();
                    if(parentActivity != null){
                        parentActivity.runOnUiThread(()->{
                            dialog.cancel();
                            if(user != null){
                                parentActivity.moveToMain();
                            }
                            else{

                            }
                        });

                    }
                    return null;
                });
            }
        });

    }
}
