package jp.sunnygem.sumaiful;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import android.view.View;
import android.widget.ProgressBar;
import com.google.firebase.FirebaseApp;
import jp.sunnygem.sumaiful.Login.NewUserFragment;
import jp.sunnygem.sumaiful.Login.SignInFragment;
import jp.sunnygem.sumaiful.Login.SignUpFragment;
import jp.sunnygem.sumaiful.Login.TopFragment;
import jp.sunnygem.sumaiful.Modal.SignInCompleteFragment;
import jp.sunnygem.sumaiful.Modal.SignUpCompleteFragment;

public class LoginActivity extends AppCompatActivity {


    private Fragment mCurrentFragment;

    private Map<String, Fragment> mFragments = new HashMap<>();

    private Stack<Fragment> mFragmentStack = new Stack<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_login);

        FirebaseApp.initializeApp(getApplicationContext());

        Client.INSTANCE.setup(getApplicationContext());
        Client.INSTANCE.setCurrentActivity(this);

        ProgressBar progress = findViewById(R.id.login_progress);
        progress.setVisibility(View.VISIBLE);

        mFragments.put("SignIn", new SignInFragment());

        Client.INSTANCE.relogin(success->{
                runOnUiThread(() -> {
                    progress.setVisibility(View.INVISIBLE);
                    if(success){
                        moveToMain();
                    }
                    else {
                        loadFragment("SignIn");
                    }
                });
            return null;
        });

    }

    public Fragment loadFragment(String key) {

        return loadFragment(mFragments.get(key));
    }

    private void _loadFragment(Fragment fragment) {
        if(mCurrentFragment == fragment){
            return;
        }

        mCurrentFragment = fragment;

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.root, fragment);
        fragmentTransaction.commit();
    }

    public Fragment loadFragment(Fragment fragment){
        mFragmentStack.push(fragment);
        _loadFragment(fragment);
        return fragment;
    }


    public void showSignedInModal(){

        SignInCompleteFragment fragment = new SignInCompleteFragment();
        fragment.setDidTouchCallback(new Runnable() {
            @Override
            public void run() {
                moveToMain();
            }
        });
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.modal_root, fragment);
        fragmentTransaction.commit();
    }


    public void showSignedUpModal(){
        SignUpCompleteFragment fragment = new SignUpCompleteFragment();
        fragment.setDidTouchCallback(new Runnable() {
            @Override
            public void run() {
                moveToMain();
            }
        });
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.modal_root, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if(mFragmentStack.size() <= 1){
            super.onBackPressed();
        }
        else {
            mFragmentStack.pop();
            Fragment f = mFragmentStack.lastElement();
            if (f != null) {
                _loadFragment(f);
            }
        }
    }

    public void moveToMain(){
        Intent next = new Intent(this, MainActivity.class);
        startActivity(next);
        finish();
    }
}