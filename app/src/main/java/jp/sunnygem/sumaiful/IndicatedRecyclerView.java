package jp.sunnygem.sumaiful;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.ViewParent;
import android.widget.ProgressBar;

public class IndicatedRecyclerView extends RecyclerView {

    private ProgressBar mProgressBar = new ProgressBar(getContext());

    public IndicatedRecyclerView(Context context) {
        super(context);
    }

    public IndicatedRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public IndicatedRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setup();
    }


    public void setLoading(boolean flg){
        if(!flg){
            mProgressBar.animate().alpha(0.f).setDuration(200).withEndAction(new Runnable() {
                @Override
                public void run() {
                    mProgressBar.setVisibility(INVISIBLE);
                }
            }).start();

        }
        else{
            mProgressBar.setAlpha(1.0f);
            mProgressBar.setVisibility(VISIBLE);
        }
//        mProgressBar.setVisibility(flg ? VISIBLE : INVISIBLE);
    }

    public void setup(){
        ViewParent parent = getParent();
        if(parent instanceof ConstraintLayout){
            ConstraintLayout layout = (ConstraintLayout) parent;
            layout.addView(mProgressBar);
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mProgressBar.getLayoutParams();
            params.startToStart = getId();
            params.endToEnd = getId();
            params.topToTop = getId();
            params.bottomToBottom = getId();
            params.horizontalBias = 0.5f;
            params.verticalBias = 0.5f;
            mProgressBar.setLayoutParams(params);

            mProgressBar.setVisibility(INVISIBLE);
        }

    }
}
