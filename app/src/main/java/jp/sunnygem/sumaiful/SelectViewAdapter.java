package jp.sunnygem.sumaiful;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class SelectViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    public interface OnSelectListener{
        void OnSelect(int index);
    }

    protected OnSelectListener mSelectListener;

    public void setOnSelectListener(OnSelectListener listener){
        mSelectListener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mSelectListener != null){
                    mSelectListener.OnSelect(position);
                }
            }
        });
    }


}
