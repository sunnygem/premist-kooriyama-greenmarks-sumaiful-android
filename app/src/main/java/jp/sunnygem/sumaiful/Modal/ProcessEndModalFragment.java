package jp.sunnygem.sumaiful.Modal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.sunnygem.sumaiful.R;

public class ProcessEndModalFragment extends Fragment {

    private Runnable mDidTouchCallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_modal_process_end, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDidTouchCallback != null) mDidTouchCallback.run();
            }
        });

        View bg = view.findViewById(R.id.bg);

        bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mDidTouchCallback != null) mDidTouchCallback.run();
            }
        });


    }

    public void setDidTouchCallback(Runnable didTouchCallback) {
        this.mDidTouchCallback = didTouchCallback;
    }
}
