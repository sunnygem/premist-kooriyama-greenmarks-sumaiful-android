package jp.sunnygem.sumaiful.Modal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import jp.sunnygem.sumaiful.R;

public class LogoutFragment extends TwoButtonsModalFragment {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title = view.findViewById(R.id.label_title);
        TextView message = view.findViewById(R.id.label_message);

        title.setText(R.string.modal_title_logout);
        message.setText(R.string.modal_message_logout);

        Button button1 = view.findViewById(R.id.button_first);
        button1.setText(R.string.modal_button_cancel);

        Button button2 = view.findViewById(R.id.button_second);
        button2.setText(R.string.modal_button_logout);
    }
}
