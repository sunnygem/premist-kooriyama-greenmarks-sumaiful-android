package jp.sunnygem.sumaiful.Modal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import jp.sunnygem.sumaiful.R;

public class LogoutCompleteFragment extends ProcessEndModalFragment {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title = view.findViewById(R.id.label_title);
        title.setText(getResources().getText(R.string.login_title_signed_out));
        TextView message = view.findViewById(R.id.label_message);
        message.setText(getResources().getText(R.string.login_message_signed_out));

    }
}
