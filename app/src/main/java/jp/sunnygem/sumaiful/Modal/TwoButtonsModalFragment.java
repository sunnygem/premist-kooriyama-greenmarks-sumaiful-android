package jp.sunnygem.sumaiful.Modal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import jp.sunnygem.sumaiful.R;

public class TwoButtonsModalFragment extends Fragment {


    Runnable mDidClickFirstButton;
    Runnable mDidClickSecondButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_modal_two_buttons, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button button1 = view.findViewById(R.id.button_first);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                didClickFirstButton();
            }
        });

        Button button2 = view.findViewById(R.id.button_second);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                didClickSecondButton();
            }
        });
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
    }

    public void setDidCLickFirstButton(Runnable r){
        mDidClickFirstButton = r;
    }
    public void setDidCLickSecondButton(Runnable r){
        mDidClickSecondButton = r;
    }

    public void didClickFirstButton(){
        if(mDidClickFirstButton != null) mDidClickFirstButton.run();
    }

    public void didClickSecondButton(){
        if(mDidClickSecondButton != null) mDidClickSecondButton.run();

    }


}
