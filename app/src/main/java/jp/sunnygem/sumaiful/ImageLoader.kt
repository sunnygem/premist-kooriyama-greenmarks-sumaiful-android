package jp.sunnygem.sumaiful

import android.Manifest
import android.R
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.widget.ArrayAdapter
import android.opengl.ETC1.getWidth
import android.opengl.ETC1.getHeight
import android.R.attr.maxLength
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.pdf.PdfRenderer
import android.media.ExifInterface
import android.media.MediaScannerConnection
import android.os.Build
import android.os.ParcelFileDescriptor
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.util.Log
import android.view.View
import jp.sunnygem.sumaiful.BuildConfig
import jp.sunnygem.sumaiful.Client
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class ImageLoader {

    companion object{
        val instance = ImageLoader()

    }

    var activity:Activity? = null
    var fragment: Fragment? = null
    var imageUri: Uri? = null
    var imagePath:String? = null
    var imageCallback:((Bitmap?)->Unit)? = null


    var permissionCallback:((Boolean)->Unit)? = null

    fun checkStoragePermission(callback:(Boolean)->Unit){
        if (ActivityCompat.checkSelfPermission(activity!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
            PackageManager.PERMISSION_GRANTED){
            callback(true)
        }
        else{
            permissionCallback = callback
            requestStoragePermission()
        }
    }

    private fun requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                Manifest.permission.CAMERA
            )
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                2
            )
        }
        else{
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                2
            )
        }
    }

    fun checkCameraPermission(callback:(Boolean)->Unit){
        if (ActivityCompat.checkSelfPermission(activity!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
            PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(activity!!,
                Manifest.permission.CAMERA) ==
            PackageManager.PERMISSION_GRANTED){
            callback(true)
        }
        else{
            permissionCallback = callback
            requestCameraPermission()
        }
    }

    private fun requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                activity!!,
                Manifest.permission.CAMERA
            )
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                2
            )
        }
        else{
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                2
            )
        }
    }

    var downloadingCount = 0

    fun saveImages(urls:List<String>, callback:(()->Unit)? = null){
        checkStoragePermission {
            if(it){
                downloadingCount = 0
                for(url in urls){
                    ++downloadingCount
                    Client.downloadData(url){
                        it?.let{
                            --downloadingCount
                            if(downloadingCount == 0)
                                saveImage(Uri.parse(url!!).lastPathSegment, it, callback)
                            else
                                saveImage(Uri.parse(url!!).lastPathSegment, it, null)
                        }
                    }
                }
            }
        }
    }

    fun toTempFileURL(bitmap:Bitmap):String{
        val byte = ImageLoader.instance.toJpeg(bitmap)
        val outputDir = activity?.getCacheDir() // context being the Activity pointer
        val outputFile = File.createTempFile("prefix", ".jpg", outputDir)
        outputFile.writeBytes(byte)
        return "file://" + outputFile.toURI().rawPath
    }

    fun saveImage(url:String?, callback:(()->Unit)? = null){
        checkStoragePermission {
            if(it){
                url?.let {
                    Client.downloadData(it){
                        it?.let{
                            saveImage(Uri.parse(url!!).lastPathSegment, it, callback)
                        }
                    }
                }
            }
        }
    }

    fun saveImage(name:String, image:ByteArray, callback:(()->Unit)? = null){
        checkStoragePermission {
            if(it){
                val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).path
                var mime = "image/jpeg"
                if(!name.contentEquals(".jpg")) mime = "image/png"
                val file = File(path + "/" + name)
                file.writeBytes(image)
                MediaScannerConnection.scanFile(activity, arrayOf(file.path), arrayOf(mime)){s, url ->
                    callback?.invoke()
                }
            }
        }
    }

    fun loadAlbum(){

        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        fragment?.startActivityForResult(photoPickerIntent, 0)
    }

    private fun loadCamera() {
        checkCameraPermission {
            if(it)
                loadCameraMain()
        }
    }
    private fun loadCameraMain(){

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.Images.Media.ORIENTATION, 0)
        val cameraFolder = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
            ), "Camera"
        )
        cameraFolder.mkdir()
//        Log.d("ImageLoader", "Camera Folder: " + cameraFolder.path)


        val fileName = SimpleDateFormat(
            "ddHHmmss", Locale.US
        ).format(Date())
        var filePath = String.format("%s/%s.jpg", cameraFolder.path, fileName)

        val cameraFile = File(filePath)
        imagePath = filePath
        val file = FileProvider.getUriForFile(fragment?.activity!!,
            fragment!!.activity!!.applicationContext.packageName + ".provider",
            cameraFile)

        intent.putExtra(
            MediaStore.EXTRA_OUTPUT,
            file
        )
        imageUri = file
        fragment?.startActivityForResult(intent, 1)
    }

    fun onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray){
        if(requestCode == 2){
            if(!grantResults.isEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                permissionCallback?.invoke(true)
            }
            else{
                permissionCallback?.invoke(false)
            }
        }
    }


    fun neededRotation(path: String): Int {
        try {

            val exif = ExifInterface(path)
            val orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL
            )

            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                return 270
            }
            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                return 180
            }
            return if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                90
            } else 0

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return 0
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun neededRotation(path: InputStream): Int {
        try {

            val exif = ExifInterface(path)
            val orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL
            )

            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                return 270
            }
            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                return 180
            }
            return if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                90
            } else 0

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return 0
    }


    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) {
            imageCallback?.invoke(null)
        }
        else if(requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    val imageUri = data?.getData()
                    var imageStream = fragment?.activity?.contentResolver?.openInputStream(imageUri)
                    var selectedImage = BitmapFactory.decodeStream(imageStream)

                    imageStream = fragment?.activity?.contentResolver?.openInputStream(imageUri)
                    var rotation = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        neededRotation(imageStream!!)
                    } else {
                        neededRotation(imageUri?.path ?: "")
                    }

                    var m = Matrix()
                    m.postRotate(rotation.toFloat())
                    selectedImage = Bitmap.createBitmap(selectedImage, 0, 0, selectedImage.width, selectedImage.height, m, true)

                    imageCallback?.invoke(selectedImage)
                } catch (e: FileNotFoundException) {
                    Log.d("ImageLoader", "Exception " + e.toString())
                    imageCallback?.invoke(null)
                }

            }
        }
        else if(requestCode == 1){
            try {
                var imageStream = fragment?.activity?.contentResolver?.openInputStream(imageUri)
                imageStream?.let{
                    var selectedImage = BitmapFactory.decodeStream(imageStream)
                    imageStream = fragment?.activity?.contentResolver?.openInputStream(imageUri)

                    var m = Matrix()
                    val r =  neededRotation(imagePath ?: "")
                    m.postRotate(r.toFloat())
                    var w = selectedImage.width
                    var h = selectedImage.height
                    selectedImage = Bitmap.createBitmap(selectedImage, 0, 0, w, h, m, true)
                    imageCallback?.invoke(selectedImage)
                }
            } catch (e: FileNotFoundException) {
                Log.d("ImageLoader", "Exception " + e.toString())
                imageCallback?.invoke(null)
            }
        }
        imageCallback = null
    }
/*
    fun loadImage(){
        val builder = AlertDialog.Builder(fragment?.context)
        builder.setTitle(fragment?.context?.resources?.getString(jp.sunnygem.willing.R.string.message_import_image))
        val selection = ArrayAdapter<String>(fragment?.context, android.R.layout.select_dialog_item)
        selection.add(fragment?.context?.resources?.getString(jp.sunnygem.willing.R.string.album))
        selection.add(fragment?.context?.resources?.getString(jp.sunnygem.willing.R.string.camera))
        builder.setNeutralButton(fragment?.context?.resources?.getString(jp.sunnygem.willing.R.string.cancel), { dialog: DialogInterface, i: Int ->
            dialog.dismiss()
        })
        builder.setAdapter(selection) { dialog: DialogInterface, i: Int ->
            if(i == 0){
                loadAlbum()
            } else if(i == 1){
                loadCamera()
            }
            dialog.dismiss()
        }

        builder.show()
    }
*/

    fun toJpeg(source:Bitmap, quality:Int = 80):ByteArray{
        val byteArrayOutputStream = ByteArrayOutputStream()
        source.compress(
            Bitmap.CompressFormat.JPEG,
            quality,
            byteArrayOutputStream
        ) //compress to 50% of original image quality
        return byteArrayOutputStream.toByteArray()
    }

    fun resizeToFitHeight(source:Bitmap, maximum:Int):Bitmap{
        try {
            val targetHeight = maximum

            val aspectRatio = source.getWidth().toDouble() / source.getHeight().toDouble()
            val targetWidth = (targetHeight * aspectRatio).toInt()

            val result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false)
            if (result != source) {
            }
            return result
        } catch (e: Exception) {
            return source
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun loadPdf(url:String, callback:(Bitmap?)->Unit){
        Client.downloadData(url) {

            ImageLoader.instance.activity?.runOnUiThread {
                it?.let {
                    loadPdf(it, 0)?.let {
                        activity?.runOnUiThread {
                            callback(it)
                        }
                    }

                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun loadPdf(data:ByteArray, page:Int):Bitmap?{
        try {
            val outputDir = activity?.getCacheDir() // context being the Activity pointer
            val outputFile = File.createTempFile("prefix", ".pdf", outputDir)
            outputFile.writeBytes(data)

            val renderer = PdfRenderer(ParcelFileDescriptor.open(outputFile, ParcelFileDescriptor.MODE_READ_ONLY))

            var bitmap: Bitmap? = null
            val pageCount = renderer.pageCount
            for (i in 0 until pageCount) {
                val page = renderer.openPage(i)

                val width = (activity?.resources?.displayMetrics?.densityDpi ?: 0) / 72 * page.width
                val height = (activity?.resources?.displayMetrics?.densityDpi ?: 0) / 72 * page.height
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                canvas.drawColor(Color.WHITE)
                page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)

                // close the page
                page.close()
                break
            }

            // close the renderer
            renderer.close()
            return bitmap
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return null
    }

    fun resizeToFit(source:Bitmap, maximum:Int):Bitmap{
        try {
            if (source.getHeight() >= source.getWidth()) {
                val targetHeight = maximum
                if (source.getHeight() <= targetHeight) { // if image already smaller than the required height
                    return source
                }

                val aspectRatio = source.getWidth() as Double / source.getHeight() as Double
                val targetWidth = (targetHeight * aspectRatio).toInt()

                val result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false)
                if (result != source) {
                }
                return result
            } else {
                val targetWidth = maximum

                if (source.getWidth() <= targetWidth) { // if image already smaller than the required height
                    return source
                }

                val aspectRatio = source.getHeight() as Double / source.getWidth() as Double
                val targetHeight = (targetWidth * aspectRatio).toInt()

                val result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false)
                if (result != source) {
                }
                return result

            }
        } catch (e: Exception) {
            return source
        }
    }
}