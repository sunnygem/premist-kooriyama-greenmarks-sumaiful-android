package jp.sunnygem.sumaiful;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.HttpAuthHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewFragment extends Fragment {

    String mUrl = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_web, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebView webView = view.findViewById(R.id.view_web);

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        //chrome inspect debuggable.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        webView.clearCache(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);


        //WebViewClient設定
        webView.setWebViewClient(new WebViewClient(){
            /** メッセージ. */
            private String mMessage = "";

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
//                Log.getInstance().debug(TAG, "onReceivedHttpAuthRequest() host:" + host + " handler:" + handler);
                //super.onReceivedHttpAuthRequest(view, handler, host, realm);
                handler.proceed("dev-sumaiful", "sgx.jp");
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if(request.isForMainFrame()) {
                    return shouldOverrideUrlLoading(view, request.getUrl().toString());
                }
                else{
                    return false;
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {

            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                //super.onReceivedError(view, request, error);
                if (request.isForMainFrame()) {
                    onReceivedError(view,
                            error.getErrorCode(), error.getDescription().toString(),
                            request.getUrl().toString());
                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                //super.onReceivedError(view, errorCode, description, failingUrl);
                /*
                Log.getInstance().error(TAG, "onReceivedError() errorCode:" + errorCode
                        + " failingUrl:" + failingUrl + " description:" + description);
                if(errorCode < 0){
                    mMessage = "（詳細：" + description + "）";
                    // 自作のエラーページを表示
                    view.loadUrl("file:///android_asset/error.html");
                }
                */
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
//                Log.getInstance().error(TAG, "onReceivedHttpError() url:" + request.getUrl().toString()
//                        + " code:" + errorResponse.getStatusCode() + " " + errorResponse.getReasonPhrase());

            }
        });

        if(mUrl != null){
            webView.loadUrl(mUrl);
        }

    }

    public void loadURL(String url){
        View view = getView();
        if(view != null) {
            WebView webView = view.findViewById(R.id.view_web);
            webView.loadUrl(url);
        }
        mUrl = url;
    }

}
