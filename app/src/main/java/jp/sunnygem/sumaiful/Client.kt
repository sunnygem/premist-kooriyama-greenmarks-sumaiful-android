package jp.sunnygem.sumaiful
import android.content.Context
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.*
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap
import android.R.id.edit
import android.annotation.SuppressLint
import android.app.Activity
import android.content.SharedPreferences
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.firebase.iid.FirebaseInstanceId
import jp.sunnygem.sumaiful.Model.*
import okio.Okio
import okio.BufferedSink
import java.io.File
import kotlin.Exception


@SuppressLint("StaticFieldLeak")
object Client{

    public var currentActivity: Activity? = null

    public var endpoint = ""
    private var encryptSecret = ""
    private var encryptSharedPassword = ""

    public val BasicId = "dev-premist-kooriyama-greenmarks-sumaiful"
    public val BasicPassword = "sgx.jp"
    private val UniqueParameter = "sumaiful"

    private var apiUrl = ""
    private val JSON = MediaType.parse("application/json; charset=utf-8")

    private var httpClient: OkHttpClient? = null

    private var isEnglish = false

    private var accessToken: String? = null
    private var refreshToken: String? = null
    public val presentDateFormat = SimpleDateFormat("yyyy.MM.dd")

    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    public var picasso: Picasso? = null

    private var context: Context? = null
    public var myUser = User()
    private var managerUser = User()
    public var deviceToken = ""

    var privacyPolicyURL:String? = null
    var privacyPolicyHtml:String? = null

    var termOfUseURL:String? = null
    var termOfUseHtml:String? = null


    external fun Encrypt(data: String, password:String, iv:String, salt:String, info:String): String

    init {

        if (BuildConfig.DEBUG) {
            //Debug
            endpoint = "https://dev-premist-kooriyama-greenmarks-sumaiful.sgx.jp/"
            encryptSecret = "dcfe967571a6b5542069c1063ea1b165d3ae2657b9d2a91706ff1ef1653aca28"
            encryptSharedPassword = "7c8776e3ceb8f646c1fb1b909dc05d58fb2f74e26a4d9bc4947e0be175d4ff39"
        }
        else {
            //Release
            endpoint = "https://sumaiful.com/"
            encryptSecret = "dcfe967571a6b5542069c1063ea1b165d3ae2657b9d2a91706ff1ef1653aca28"
            encryptSharedPassword = "7c8776e3ceb8f646c1fb1b909dc05d58fb2f74e26a4d9bc4947e0be175d4ff39"
        }
        apiUrl = endpoint + "api/v1/0/"


        System.loadLibrary("crypto")
        System.loadLibrary("ssl")
        System.loadLibrary("Encrypt")
        myUser.id = 6
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { result ->
            deviceToken = result.token
        }

    }

    private fun responseCount(response: Response):Int{
        var res: Response? = response.priorResponse()
        var cnt = 0
        do{
            cnt += 1
            res = res?.priorResponse()
        }while(res != null)
        return cnt
    }

    fun buildHttpClient(): OkHttpClient{
        val builder = OkHttpClient.Builder()
        builder.authenticator(Authenticator { route, response ->

            if(responseCount(response) >= 3){
                return@Authenticator null;
            }
            val credential = Credentials.basic(BasicId, BasicPassword)
            return@Authenticator response.request().newBuilder().header("Authorization", credential).build()

        })
        val cache = File(context!!.getApplicationContext().cacheDir, "httpcache")

        builder.cache(Cache(cache, 1024*1024*100))
        builder.connectTimeout(10, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        return builder.build()
    }

    fun didLogin(){
        fetchTermOfUse { html, url, exception ->
            termOfUseHtml = html
            termOfUseURL = url
        }
        fetchPrivacyPolicy { html, url, exception ->
            privacyPolicyHtml = html
            privacyPolicyURL = url
        }
    }

    fun setup(context: Context){
        this.context = context
        httpClient = buildHttpClient()
        val downloader = OkHttp3Downloader(httpClient)
        picasso = Picasso.Builder(context).downloader(downloader).build()

        load()
    }

    fun relogin(callback:(success:Boolean)->Unit){
        refreshAccessToken { err ->
            err?.let {
                callback(false)
                return@refreshAccessToken
            }

            fetchUser(null) { user, error ->
                user?.let {
                    myUser = it
                    didLogin()
                    callback(true)
                    return@fetchUser
                }
                callback(false)
            }
        }
    }
    fun logout(){
        accessToken = null
        refreshToken = null
        save()
    }

    fun saveFavorite(application: Application){
        val preferences = context?.getSharedPreferences("Favorite", Context.MODE_PRIVATE)
        val editor = preferences?.edit()
        editor?.putBoolean(application.appId, application.isFavorite)
        editor?.commit()
    }
    fun loadFavorite(application: Application){
        val preferences = context?.getSharedPreferences("Favorite", Context.MODE_PRIVATE)
        application.isFavorite = preferences?.getBoolean(application.appId, false) ?: false
    }

    fun save(){
        val preferences = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        val editor = preferences?.edit()
        accessToken.let {
            editor?.putString("AccessToken", it)
        }
        refreshToken.let {
            editor?.putString("RefreshToken", it)
        }
        editor?.commit()
    }
    fun load(){
        val preferences = context?.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE)
        accessToken = preferences?.getString("AccessToken", null)
        refreshToken = preferences?.getString("RefreshToken", null)
    }


    fun fetchPrivacyPolicy(callback:(html:String?, url:String?, Exception?)->Unit){
        call("privacy_policy", HashMap()){ list, e ->
            var html:String? = null
            var url:String? = null
            list.firstOrNull()?.let{
                if(isEnglish){
                    html = it["content_en"] as? String
                    url = it["url_en"] as? String
                }
                else{
                    html = it["content"] as? String
                    url = it["url"] as? String
                }
                callback(html, url, e)
            }
        }
    }


    fun fetchTermOfUse(callback:(html:String?, url:String?, Exception?)->Unit){
        call("terms_of_service", HashMap()){ list, e ->
            var html:String? = null
            var url:String? = null
            list.firstOrNull()?.let{
                if(isEnglish){
                    html = it["content_en"] as? String
                    url = it["url_en"] as? String
                }
                else{
                    html = it["content"] as? String
                    url = it["url"] as? String
                }
                callback(html, url, e)
            }
        }
    }
    fun fetchAccessToken(callback:(e:Exception?)->Unit){
        val req = Request.Builder().url(apiUrl + "get_token").post(RequestBody.create(JSON, jacksonObjectMapper().writeValueAsString(mapOf("unique_parameter" to UniqueParameter)))).build()
        httpClient?.newCall(req)?.enqueue(object:Callback{
            override fun onFailure(call: Call, e: IOException) {
                callback(e)
            }

            override fun onResponse(call: Call, response: Response) {
                try {
                    response.body()?.string()?.let {
                        val result = jacksonObjectMapper().readValue<HashMap<String, Any>>(it)

                        (result["data"] as? Map<String, Any>)?.let {
                            accessToken = it["access_token"] as? String
                            refreshToken = it["refresh_token"] as? String
                            save()
                        }

                        print(result)
                    }

                    callback(null)
                }
                catch(e: Exception){
                    callback(e)
                }
            }

        })
    }

    fun refreshAccessToken(callback:(e:Exception?)->Unit){
        if(refreshToken == null){
            fetchAccessToken { e ->
                if(e != null){
                    callback(e)
                }
                else{
                    refreshAccessToken(callback)
                }
            }
            return
        }



        val req = Request.Builder().url(apiUrl + "get_refresh_token")
            .post(RequestBody.create(JSON, jacksonObjectMapper().writeValueAsString(mapOf("unique_parameter" to UniqueParameter, "refresh_token" to refreshToken)))).build()
        httpClient?.newCall(req)?.enqueue(object:Callback{
            override fun onFailure(call: Call, e: IOException) {
                callback(e)
            }

            override fun onResponse(call: Call, response: Response) {
                try {
                    response.body()?.string()?.let {
                        val result = jacksonObjectMapper().readValue<HashMap<String, Any>>(it)
                        (result["data"] as? Map<String, Any>)?.let {
                            accessToken = it["access_token"] as? String
                            refreshToken = it["refresh_token"] as? String
                            save()
                        }

                        print(result)
                    }
                    if(accessToken == null){
                        refreshToken = null
                    }
                    callback(null)
                }
                catch (e:Exception){
                    callback(e)
                }
            }

        })
    }

    private fun parameters(extra:Map<String, Any>):String{
        var map = hashMapOf<String, Any>("access_token" to (accessToken ?: ""), "unique_parameter" to UniqueParameter)
        map.putAll(extra)
        return jacksonObjectMapper().writeValueAsString(map)
    }

    private fun callRequest(request:Request, callback:(List<Map<String,Any>>, e:Exception?)->Unit, retryCallback:()->Unit){
        if(accessToken == null){
            refreshAccessToken { e ->
                if(e != null) callback(ArrayList(), e)
                else retryCallback()
            }
            return
        }

        httpClient?.newCall(request)?.enqueue(object:Callback{
            override fun onFailure(call: Call, e: IOException) {
                callback(ArrayList(), e)
            }

            override fun onResponse(call: Call, response: Response) {
                var list = ArrayList<Map<String, Any>>()
                try {
                    response.body()?.string()?.let {
                        val result = jacksonObjectMapper().readValue<HashMap<String, Any>>(it)

                        val codeStr = result.get("code") as? String ?: "0"
                        var code = codeStr.toInt()
                        if (code == 1) {
                            accessToken = null
                            retryCallback()
//                        callRequest(request, callback)
                            return
                        } else if (code == 2) {
                            fetchAccessToken {
                                if (it != null) {
                                    callback(list, it)
                                    return@fetchAccessToken
                                }
                                retryCallback()
//                            callRequest(request, callback)
                            }
                            return
                        } else if (code != 0 && code != 200) {
                            callback(list, Exception("error code " + code.toString()))
                            return
                        }



                        (result["data"] as? List<*>)?.let {
                            for (item in it) {
                                (item as? Map<String, Any>)?.let {
                                    list.add(it)
                                }
                            }
                        }
                        (result["data"] as? Map<String, Any>)?.let {
                            list.add(it)
                        }
                    }

                    callback(list, null)
                }
                catch(e: Exception){
                    callback(list, e)
                }
            }
        })
    }

    private fun call(url:String, parameters:Map<String, Any>, callback:(List<Map<String,Any>>, e:Exception?)->Unit){
        val req = Request.Builder().url(apiUrl + url).post(RequestBody.create(JSON, parameters(parameters))).build()
        callRequest(req, callback){
            call(url, parameters, callback)
        }
    }



    fun fetchEncryptData(callback:(Int, String, String, Exception?)->Unit){
        call("get_encryption_parameters", HashMap()) { list, e ->
            list.firstOrNull()?.let {
                val id = it["id"] as? Int ?: -1
                val iv = it["iv"] as? String ?: ""
                val salt = it["salt"] as? String ?: ""
                callback(id, iv, salt, e)
                return@call
            }
            callback(0, "", "", e)
        }
    }

    fun login(loginId:String, password:String, callback:(User?, Exception?)->Unit){
        fetchEncryptData { id, iv, salt, exception ->
            exception?.let {
                callback(null, it)
                return@fetchEncryptData
            }

            val secret = encryptSecret
            val sharedPass = encryptSharedPassword
            val encrypted = Encrypt(secret+password, sharedPass, iv, salt, "")

            call("login",  mapOf("login_id" to loginId, "password" to encrypted,
                "encryption_parameter_id" to id, "device_token" to deviceToken, "os" to "android")){ list, e ->
                list.firstOrNull()?.let{
                    val u = parseUser(it)
                    u?.let{
                        myUser = it
                    }
                    didLogin()
                    callback(u, e)
                    return@call
                }
                callback(null, e)
            }
        }
    }


    fun fetchUser(userId:Int?, callback:(user:User?, e:Exception?)->Unit){
        var map = HashMap<String, Any>()
        userId?.let {
            map["user_id"] = it
        }
        call("get_front_user", map) { list, e ->
            if(list.isEmpty())
                callback(null, e)
            else
                callback(parseUser(list.first()), e)
        }
    }

    private fun parseManualCategory(data:Any?):ManualCategory? {
        (data as? Map<String, Any>)?.let {
            val category = ManualCategory()

            category.id = it["id"] as? Int ?: 0
            category.url = parseUrl(it["icon_path"])
            category.title = it["name"] as? String ?: ""
            return category
        }
        return null
    }

    private fun parseManual(data:Any?):Concierge.Manual? {
        (data as? Map<String, Any>)?.let {
            val manual = Concierge.Manual()
            manual.date = parseDate(it["created"])
            (it["equipment_name"] as? String)?.let{
                manual.name = it
            }
            for(d2 in it["files"] as? List<Map<String,Any>> ?: ArrayList()) {
                val file = parseUrl(d2["file_path"])
                file?.let{
                    manual.files.add(it)
                }
            }
            return manual
        }
        return null
    }

    private fun parseUser(data:Any?):User?{
        (data as? Map<String, Any>)?.let{
            val user = User()
            user.id = it["id"] as? Int ?: 0
            user.name = it["nickname"] as? String ?: ""
            val displayData = it["front_user"] as? Map<String, Any> ?:HashMap()
            if(user.name.isEmpty())
                user.name = displayData["nickname"] as? String ?: ""
            user.type = displayData["type"] as? Int ?: 0


            return user
        }
        return null
    }

    private fun parseUrl(path: Any?): String? {
        (path as? String)?.let {
            return endpoint + it
        }
        return null
    }

    private fun parseDate(date: Any?): Date {
        var date = date as? String
        date = date?.replace("T", " ")
        date?.let {
            try {
                return dateFormat.parse(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }

        return Date()
    }

    private fun parseInformation(data:Map<String, Any>?):Information?{
        data?.let{
            val date = parseDate(it["release_date"])
            val title = data["title"] as String?

            val information = Information(title, date)

            val images = data["images"]
            if (images != null) {
                val imageList = images as List<Map<String, Any>>?
                for (image in imageList!!) {
                    val name = image["name"] as String?
                    val path = parseUrl(image["file_path"])
                    information.addImage(name, path)
                }
            }
            val pdfs = data["pdfs"]
            if (pdfs != null) {
                val pdfList = pdfs as List<Map<String, Any>>?
                for (pdf in pdfList!!) {
                    val name = pdf["name"] as String?
                    val path = parseUrl(pdf["file_path"])
                    information.addPdf(name, path)
                }
            }

            information.content = data["content"] as? String ?: ""

            val link = data["link_url"]
            if (link != null) {
                var linkStr: String = link as? String ?: ""
                if (linkStr.startsWith("//")) {
                    linkStr = "https:$linkStr"
                }
                information.link = linkStr
            }
            return information
        }
        return null
    }

    fun fetchManualCategories(callback:(List<ManualCategory>, Exception?)->Unit){
        call("manuals", mapOf("contractant_service_menu_id" to 41)) { list, e ->
            val infoList = ArrayList<ManualCategory>()
            for(data in list){
                parseManualCategory(data)?.let{
                    infoList.add(it)
                }
            }

            callback(infoList, e)
        }
    }

    fun fetchManuals(category:ManualCategory, callback:(List<Concierge.Manual>, Exception?)->Unit){
        call("manuals", mapOf("contractant_service_menu_id" to 41, "category_id" to category.id)) { list, e ->
            val infoList = ArrayList<Concierge.Manual>()
            for(data in list){
                parseManual(data)?.let{
                    infoList.add(it)
                }
            }

            callback(infoList, e)
        }

    }

    fun fetchTopInformation(callback:(List<Information>, Exception?)->Unit){
        call("top", mapOf("device_token" to deviceToken, "os" to "android", "user_type" to myUser.type.toString())) { list, e ->
            val infoList = ArrayList<Information>()
            if(!list.isEmpty()){
                val data = list[0]
                (data["information"] as? List<Map<String, Any>>)?.let {
                    for(data in it){
                        parseInformation(data)?.let{
                            infoList.add(it)
                        }
                    }
                }
            }

            callback(infoList, e)
        }
    }


    private fun parseMoreSumaiful(data:Map<String, Any>?):MoreSumaiful? {
        data?.let{
            val title = it["title"] as String?

            val date = parseDate(it["release_date"])
            val ms = MoreSumaiful(title, date)

            val images = it["files"]
            if (images != null) {
                val imageList = images as List<Map<String, Any>>?
                for (image in imageList!!) {
                    val name = image["name"] as String?
                    val path = image["file_path"] as String?
                    ms.addImage(name, parseUrl(image["file_path"]))
                }
            }
            ms.contents = it["html_content"] as? String
            return ms
        }
        return null
    }


    fun fetchMoreSumaiful(callback:(List<MoreSumaiful>, Exception?)->Unit){
        call("more_sumaiful", HashMap()) { list, e ->
            val infoList = ArrayList<MoreSumaiful>()

            for(data in list){
                parseMoreSumaiful(data)?.let{
                    infoList.add(it)
                }
            }
            callback(infoList, e)
        }
    }

    private fun parseAppList(list: List<Map<String, Any>>): List<Application> {

        val result = ArrayList<Application>()
        for (app in list) {

            val appObj = Application()
            appObj.scheme = app["scheme_android"] as? String ?: ""
            appObj.title = app["name"] as? String ?: ""
            appObj.storeUrl = app["store_url_android"] as? String ?: ""
            val appId = app["app_id_ios"] as? String ?: ""
            if(appId.length < 2) continue;
            appObj.appId = appId.substring(2)
            loadFavorite(appObj)

            result.add(appObj)
        }

        result.sortByDescending {
            it.isFavorite
        }

        return result
    }

    private fun parseAccessory(data:Map<String, Any>?):Accessory? {
        data?.let{
            val title = data.get("label") as String?
            val subtitle = data.get("label_en") as String?
            val accessory = Accessory(title, subtitle)
            accessory.memo = data.get("memo") as String? ?: ""
            val obj = data.get("menu_applications")
            accessory.iconUrl =this.parseUrl(data["service_menu_icon_path"])
            if (obj is List<*>) {
                accessory.appList = parseAppList((data.get("menu_applications") as List<Map<String, Any>>?)!!)
            }
            return accessory
        }
        return null
    }


    fun fetchAllAccessories(callback:(List<Accessory>, Exception?)->Unit){
        call("homekit", mapOf("page" to 0, "limit" to "20")) { list, e ->
            val infoList = ArrayList<Accessory>()

            for(data in list){
                parseAccessory(data)?.let{
                    infoList.add(it)
                }
            }
            callback(infoList, e)
        }
    }

    private fun parseConcierge(data:Map<String, Any>?):Concierge? {
        data?.let{
            val title = data.get("label") as? String ?: ""
            val subtitle = data.get("label_en") as? String ?: ""
            val concierge = Concierge()
            concierge.name = title
            concierge.information = subtitle
            concierge.type = data["service_menu_id"] as? Int ?: 0
            concierge.iconUrl =this.parseUrl(data["service_menu_icon_path"])


            when(concierge.type){
                1 -> {  //ContactPhone
                    val subdata = Concierge.Contacts()
                    (data["menu_contact_tel"] as? Map<String,Any>)?.let{ data ->
                        (data["tel"] as? String)?.let{
                            subdata.tel = it
                        }
                        (data["property_url_label"] as? String)?.let{
                            subdata.title = it
                        }
                        (data["property_url"] as? String)?.let{
                            subdata.url = it
                        }
                        (data["attend_reserve_url_label"] as? String)?.let{
                            subdata.reserveTitle = it
                        }
                        (data["attend_reserve_url"] as? String)?.let{
                            subdata.reserveUrl = it
                        }
                        (data["comment"] as? String)?.let{
                            subdata.comment = it
                        }
                    }
                    concierge.data = subdata
                }

                6 -> {  //App
                    val subdata = Concierge.Applications()
                    subdata.apps.addAll(parseAppList(data["menu_applications"] as? List<Map<String,Any>> ?: ArrayList()))
                    subdata.memo = data["memo"] as? String ?: ""

                    concierge.data = subdata
                }

                7 -> {  //Manual
                    val subdata = Concierge.Manuals()
                    for(d in data["menu_manuals"] as? List<Map<String,Any>> ?: ArrayList()) {
                        val obj = Concierge.Manual()
                        obj.date = parseDate(d["created"])
                        (d["equipment_name"] as? String)?.let{
                            obj.name = it
                        }
                        for(d2 in d["files"] as? List<Map<String,Any>> ?: ArrayList()) {
                            val file = parseUrl(d2["file_path"])
                            file?.let{
                                obj.files.add(it)
                            }
                        }
                        subdata.manuals.add(obj)
                    }
                    concierge.data = subdata
                }

                9 -> {  //Survey
                    val subdata = Concierge.Surveys()
                    for(d in data["menu_enquetes"] as? List<Map<String,Any>> ?: ArrayList()) {
                        val obj = Concierge.Survey()
                        obj.id = d["it"] as? Int ?: 0
                        obj.title = d["title"] as? String ?: ""
                        subdata.surveys.add(obj)
                    }
                    concierge.data = subdata
                }

                11 -> { //Link
                    val subdata = Concierge.Links()
                    for(tel in data["menu_linktypes"] as? List<Map<String,Any>> ?: ArrayList()) {
                        (tel["link_url"] as? String)?.let{
                            subdata.links.add(it)
                        }
                    }
                    concierge.data = subdata
                }
            }
            concierge.data?.name = concierge.name
            return concierge
        }
        return null
    }

    fun fetchAllConcierges(callback:(List<Concierge>, Exception?)->Unit){
        call("concierge", mapOf("page" to 0, "limit" to "20", "user_type" to myUser.type)) { list, e ->
            val infoList = ArrayList<Concierge>()

             for(data in list){
                parseConcierge(data)?.let{
                    if(myUser.type != 1){   //Not resident users
                        if(it.type == 7)    //Avoid manual
                            return@let
                    }
                    infoList.add(it)
                }
            }
            callback(infoList, e)
        }
    }


    fun fetchIconUrl(appId:String, callback:(String?, Exception?)->Unit){
        val request = Request.Builder().url("https://itunes.apple.com/jp/lookup?id=" + appId).get().build()
        httpClient?.newCall(request)?.enqueue(object:Callback{
            override fun onFailure(call: Call, e: IOException) {
                callback(null, e)
            }

            override fun onResponse(call: Call, response: Response) {
                var url:String? = null
                response.body()?.string()?.let {
                    val result = jacksonObjectMapper().readValue<HashMap<String,Any>>(it)
                    val items = result["results"] as? ArrayList<HashMap<String, Any>>
                    for(item in items ?: ArrayList()){
                        val artwork = item["artworkUrl512"] as? String
                        artwork?.let{
                            url = it
                            callback(url, null)
                            return
                        }
                        val artwork2 = item["artworkUrl60"] as? String
                        artwork2?.let{
                            url = it
                            callback(url, null)
                            return
                        }
                        val artwork3 = item["artworkUrl100"] as? String
                        artwork3?.let{
                            url = it
                            callback(url, null)
                            return
                        }

                    }
                    callback(url, null)
                }

            }
        })

    }

    fun downloadData(url:String, callback:(ByteArray?)->Unit){
        val req = Request.Builder().url(url).get().build()
        httpClient?.newCall(req)?.enqueue(object:Callback{
            override fun onFailure(call: Call, e: IOException) {
                callback(null)
            }

            override fun onResponse(call: Call, response: Response) {
                callback(response.body()?.bytes())
            }
        })
    }


    fun download(url: String, callback: (File?, Exception?)->Unit) {

        val name = url.substring(url.lastIndexOf('/') + 1)
        val req = Request.Builder().url(url).get().build()
        httpClient?.newCall(req)?.enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(null, e)
            }

            override fun onResponse(call: Call, response: Response) {

                val downloadedFile = File(context?.getCacheDir(), name)
                val sink = Okio.buffer(Okio.sink(downloadedFile))
                sink.writeAll(response.body()?.source())
                sink.close()
                callback(downloadedFile, null)
            }
        })

    }

}