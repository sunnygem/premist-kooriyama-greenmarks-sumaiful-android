package jp.sunnygem.sumaiful.Model;

import java.util.ArrayList;
import java.util.List;

public class SelectSurvey extends Survey{

    public class Selection{
        private String mTitle;
        private boolean mIsSelected = false;
        Selection(String title){
            mTitle = title;
        }

        public String getTitle(){
            return mTitle;
        }

        public boolean isSelected() {
            return mIsSelected;
        }

        public void setSelected(boolean selected) {
            mIsSelected = selected;
        }
    }

    List<Selection> mSelections = new ArrayList<>();

    private boolean mCanMultiSelect = false;

    public SelectSurvey(String title, String date, boolean canMultiSelect) {
        super(title, date);
        mCanMultiSelect = canMultiSelect;
    }


    public boolean canMultiSelect() {
        return mCanMultiSelect;
    }

    public void setCanMultiSelect(boolean canMultiSelect) {
        this.mCanMultiSelect = canMultiSelect;
    }

    public void addSelection(String title){
        mSelections.add(new Selection(title));
    }

    public Selection getSelection(int idx){
        return mSelections.get(idx);
    }

    public int getSelectionCount(){
        return mSelections.size();
    }

}


;