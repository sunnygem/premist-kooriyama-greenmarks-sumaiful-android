package jp.sunnygem.sumaiful.Model;

public class NumberSelectSurvey extends SelectSurvey {

    public NumberSelectSurvey(String title, String date, int count) {
        super(title, date, false);
        for(int i = 0; i<count; ++i){
            addSelection(String.valueOf(i+1));
        }
    }
}
