package jp.sunnygem.sumaiful.Model;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MoreSumaiful {

    public class Image{
        String mName;
        String mUrl;
        Image(String name, String url){
            mName = name;
            mUrl = url;
        }

        public String getName() {
            return mName;
        }
        public String getUrl() {
            return mUrl;
        }
    }

    private String mTitle;
    private String mContents;
    private Date mDate;

    private List<Image> mImages = new ArrayList<>();

    public MoreSumaiful(String title, Date date){
        mTitle = title;
        mDate = date;
    }

    public Date getDateObject(){
        return mDate;
    }
    public void setDateObject(Date date){
        mDate = date;
    }

    public String getTitle(){return mTitle;}
    public Date getDate(){return mDate;}

    public void addImage(String name, String url){
        mImages.add(new Image(name, url));
    }

    public List<Image> getImages() {
        return mImages;
    }

    public String getContents() {
        return mContents;
    }

    public void setContents(String contents) {
        mContents = contents;
    }
}
