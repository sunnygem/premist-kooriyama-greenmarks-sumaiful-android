package jp.sunnygem.sumaiful.Model;

public class Specs {

    private String mTitle;
    private String mDate;
    private boolean hasPdf;


    public Specs(String title, String date, boolean pdf){
        mTitle = title;
        mDate = date;
        hasPdf = pdf;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public boolean hasPdf() {
        return hasPdf;
    }

    public void setHasPdf(boolean hasPdf) {
        this.hasPdf = hasPdf;
    }




}
