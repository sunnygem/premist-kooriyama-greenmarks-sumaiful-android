package jp.sunnygem.sumaiful.Model;

import java.util.ArrayList;
import java.util.List;

public class Communication {

    public static class Message{

        private User mUser;
        private String mDate;
        private String mContents;
        private boolean mIsIncoming;

        public User getUser() {
            return mUser;
        }

        public void setUser(User user) {
            mUser = user;
        }

        public String getDate() {
            return mDate;
        }

        public void setDate(String date) {
            mDate = date;
        }

        public String getContents() {
            return mContents;
        }

        public void setContents(String contents) {
            mContents = contents;
        }

        public boolean isIncoming(){
            return mIsIncoming;
        }
        public void setIsIncoming(boolean flg){mIsIncoming = flg;};

    };

    public static class Thread{

        private Communication mCommunication;
        private List<Message> mMessages = new ArrayList<>();

        private int mId;
        private String mTitle;
        private String mDate;
        private User mUser;

        public Thread(Communication c){
            mCommunication = c;
        }

        public List<Message> getMessages() {
            return mMessages;
        }

        public Communication getCommunication() {
            return mCommunication;
        }

        public int getId() {
            return mId;
        }

        public void setId(int id) {
            mId = id;
        }

        public String getTitle() {
            return mTitle;
        }

        public String getDate() {
            return mDate;
        }

        public void setDate(String date) {
            mDate = date;
        }

        public void setTitle(String title) {
            mTitle = title;
        }

        public User getUser() {
            return mUser;
        }

        public void setUser(User user) {
            mUser = user;
        }
    };


    private List<Thread> mThreads = new ArrayList<>();
    private String mTitle;
    private int mId;

    public Communication(String title){
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public List<Thread> getThreads() {
        return mThreads;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

}
