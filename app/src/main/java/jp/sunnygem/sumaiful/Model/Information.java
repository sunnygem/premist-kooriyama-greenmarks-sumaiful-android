package jp.sunnygem.sumaiful.Model;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Information {

    public class Image{
        String mName;
        String mUrl;
        Image(String name, String url){
            mName = name;
            mUrl = url;
        }

        public String getName() {
            return mName;
        }
        public String getUrl() {
            return mUrl;
        }
    }

    public class Pdf{
        String mName;
        String mUrl;
        Pdf(String name, String url){
            mName = name;
            mUrl = url;
        }
        public String getName() {
            return mName;
        }

        public String getUrl() {
            return mUrl;
        }
    }

    private String mTitle;
    private String mLink;
    private String mContent;
    private Date mDate;

    private List<Image> mImages = new ArrayList<>();
    private List<Pdf> mPdfs = new ArrayList<>();

    public Information(String title, Date date){
        mTitle = title;
        mDate = date;
    }

    public String getTitle(){return mTitle;}
    public Date getDate(){return mDate;}

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        if(link != null && !link.isEmpty())
            mLink = link;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        this.mContent = content;
    }

    public void addImage(String name, String url){
        mImages.add(new Image(name, url));
    }
    public void addPdf(String name, String url){
        mPdfs.add(new Pdf(name, url));
    }

    public List<Image> getImages() {
        return mImages;
    }

    public List<Pdf> getPdfs() {
        return mPdfs;
    }
}
