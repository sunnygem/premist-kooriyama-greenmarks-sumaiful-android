package jp.sunnygem.sumaiful.Model

import java.util.*

class Concierge {
    open class Data{
        var name = ""
    }
    class Contacts: Data() {
        var tel = ""
        var title = ""
        var url = ""
        var reserveTitle = ""
        var reserveUrl = ""
        var comment = ""
    }
    class Applications: Data(){
        var iconUrl: String = ""
        var memo =  ""
        var apps = ArrayList<Application>()
    }
    class Manual{
        var name = ""
        var date = Date()
        var files = ArrayList<String>()
    }
    class Manuals: Data(){
        var manuals = ArrayList<Manual>()
    }
    class Survey: Data(){
        var title = ""
        var id = 0
    }

    class Surveys: Data(){
        var surveys = ArrayList<Survey>()
    }
    class Links: Data(){
        var links = ArrayList<String>()
    }


    var name = ""
    var information = ""
    var iconUrl:String? = null
    var type = 0
    var data:Data? = null
}
