package jp.sunnygem.sumaiful.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Application(var title: String = "",
                       var scheme: String = "",
                       var storeUrl: String = "",
                       var appId: String = "",
                       var isFavorite: Boolean = false) :
    Parcelable
