package jp.sunnygem.sumaiful.Model;

import java.util.ArrayList;
import java.util.List;

public class Accessory {
    String mTitle = "";
    String mSubtitle = "";

    String mIconUrl = "";
    String mMemo = "";

    List<Application> mAppList = new ArrayList<>();

    public Accessory(String title, String subtitle){
        mTitle = title;
        mSubtitle = subtitle;
    }
    public String getTitle(){return mTitle;};
    public String getSubtitle(){return mSubtitle;};

    public List<Application> getAppList() {
        return mAppList;
    }

    public void setAppList(List<Application> appList) {
        mAppList = appList;
    }


    public String getIconUrl() {
        return mIconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.mIconUrl = iconUrl;
    }

    public String getMemo() {
        return mMemo;
    }

    public void setMemo(String memo) {
        this.mMemo = memo;
    }
}

