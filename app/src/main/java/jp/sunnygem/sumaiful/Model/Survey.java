package jp.sunnygem.sumaiful.Model;

public class Survey {

    private String mTitle;
    private String mDate;
    private String mContents;
    private int mIndex_;


    public Survey(String title, String date){
        mTitle = title;
        mDate = date;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

}


