package jp.sunnygem.sumaiful.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class User(var id:Int = 0,
                var name: String = "",
                var type:Int = 0) :
    Parcelable