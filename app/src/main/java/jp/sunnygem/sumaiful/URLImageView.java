package jp.sunnygem.sumaiful;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.ViewParent;
import android.widget.ProgressBar;
import com.squareup.picasso.Callback;

public class URLImageView extends android.support.v7.widget.AppCompatImageView implements Callback {

    private ProgressBar mProgressBar = new ProgressBar(getContext());
    private boolean isLoading = false;
    public URLImageView(Context context) {
        super(context);
    }

    public URLImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public URLImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void setup(){
        if(mProgressBar.getParent() == null) {
            ViewParent parent = getParent();
            if (parent instanceof ConstraintLayout) {
                ConstraintLayout layout = (ConstraintLayout) parent;
                layout.addView(mProgressBar);
                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mProgressBar.getLayoutParams();
                params.startToStart = getId();
                params.endToEnd = getId();
                params.topToTop = getId();
                params.bottomToBottom = getId();
                params.horizontalBias = 0.5f;
                params.verticalBias = 0.5f;
                mProgressBar.setLayoutParams(params);
                if(!isLoading)
                    mProgressBar.setVisibility(INVISIBLE);
            }
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setup();
    }


    public void loadUrl(String url){
        if(url == null || url.isEmpty()) return;
        isLoading = true;
        mProgressBar.setAlpha(1.0f);
        mProgressBar.setVisibility(VISIBLE);
        Client.INSTANCE.getPicasso().load(url).into(this, this);
    }

    @Override
    public void onSuccess() {
        mProgressBar.animate().alpha(0.f).setDuration(200).withEndAction(new Runnable() {
            @Override
            public void run() {
                mProgressBar.setVisibility(INVISIBLE);
            }
        }).start();

        isLoading = false;
    }

    @Override
    public void onError(Exception e) {
        mProgressBar.animate().alpha(0.f).setDuration(200).withEndAction(new Runnable() {
            @Override
            public void run() {
                mProgressBar.setVisibility(INVISIBLE);
            }
        }).start();

        isLoading = false;
    }
}
