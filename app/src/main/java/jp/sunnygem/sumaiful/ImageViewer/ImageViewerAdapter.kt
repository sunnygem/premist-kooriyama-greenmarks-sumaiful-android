package jp.sunnygem.sumaiful.ImageViewer

import android.net.Uri
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.davemorrissey.labs.subscaleview.ImageSource
import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator
import com.github.piasy.biv.view.BigImageView
import com.github.piasy.biv.view.GlideImageViewFactory
import jp.sunnygem.sumaiful.R
import java.io.File

class ImageViewerAdapter : RecyclerView.Adapter<ImageViewerAdapter.ViewHolder>() {


    var urls = ArrayList<String>()
    private val viewFactory = GlideImageViewFactory()
    var currentItemView:ImageViewerAdapter.ViewHolder? = null

    override fun getItemCount(): Int {
        return urls.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_item_image_viewer, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(urls?.get(position))
        currentItemView = holder
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.clear()
    }

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder.hasNoImage()) {
            holder.rebind()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var url:String? = null
        val itemImage: BigImageView

        init {
            itemImage = itemView.findViewById(R.id.itemImage)
            itemImage.setProgressIndicator(ProgressPieIndicator())
            itemImage.setTapToRetry(true)
            itemImage.setImageViewFactory(viewFactory)
        }

        fun bind(url: String?) {
            this.url = url
            if(url != null) {
                url?.let{url ->

                    /*
                    if(url.endsWith("pdf") == true){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ImageLoader.instance.loadPdf(url) {
                                it?.let{
                                    val url = ImageLoader.instance.toTempFileURL(it)
                                    itemImage.showImage(Uri.parse(url))
                                }
                            }
                        }
                    }
                    else {
                    }
                    */
                    itemImage.showImage(Uri.parse(url))
                }
            }
        }

        fun rebind() {
            if(url != null) {
                itemImage.showImage(Uri.parse(url))
            }
        }

        fun clear() {
            val ssiv = itemImage.ssiv
            ssiv?.recycle()
        }

        fun hasNoImage(): Boolean {
            val ssiv = itemImage.ssiv
            return ssiv == null || !ssiv.hasImage()
        }
    }
}
