package jp.sunnygem.sumaiful.ImageViewer

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.LinearSnapHelper
import android.support.v7.widget.RecyclerView
import android.widget.ArrayAdapter
import android.widget.ImageButton
import com.github.piasy.biv.BigImageViewer
import com.github.piasy.biv.loader.glide.GlideCustomImageLoader
import jp.sunnygem.sumaiful.Client
import jp.sunnygem.sumaiful.R

class ImageViewerActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)
        BigImageViewer.initialize(GlideCustomImageLoader.with(applicationContext, Client.buildHttpClient()))

//        ImageLoader.instance.activity = this

        val recycler = findViewById<RecyclerView>(R.id.view_recycler)
        recycler.setLayoutManager(
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        )

        val enableDownload = intent.getBooleanExtra("EnableDownload", true)
        val enableReport = intent.getBooleanExtra("EnableReport", true)

        val adapter = ImageViewerAdapter()

        adapter.urls.add(intent.getStringExtra("url"))
        /*
        adapter.album = intent.getParcelableExtra<Album>("data")
        if(adapter.album == null) {
            val file = intent.getParcelableExtra<ImageFile>("file")
            val album = Album()
            album.files.add(file)
            adapter.album = album
        }
        val index = intent.getIntExtra("index", 0)
        */
        recycler.setAdapter(adapter)

        val closeButton = findViewById<ImageButton>(R.id.button_close)
        closeButton?.setOnClickListener {
            finish()
        }


        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(recycler)
        recycler.scrollToPosition(0)
    }

}