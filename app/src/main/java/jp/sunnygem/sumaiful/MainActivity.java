package jp.sunnygem.sumaiful;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Modal.*;
import jp.sunnygem.sumaiful.SideMenu.DisclaimerFragment;
import jp.sunnygem.sumaiful.SideMenu.EmailChangeFragment;
import jp.sunnygem.sumaiful.SideMenu.FamilyKeyGenerationFragment;
import jp.sunnygem.sumaiful.SideMenu.PasswordChangeFragment;
import jp.sunnygem.sumaiful.SideMenu.MenuFragment;
import jp.sunnygem.sumaiful.SideMenu.ProfileEditFragment;
import jp.sunnygem.sumaiful.SideMenu.SideMenuSubFragment;
import jp.sunnygem.sumaiful.Tab.TabBar;
import jp.sunnygem.sumaiful.Tab.TabRootFragment;
import jp.sunnygem.sumaiful.Tab.Top.TopFragment;

public class MainActivity extends AppCompatActivity implements TabBar.TabBarListener, MenuFragment.CloseMenuListener, MenuFragment.SelectItemListener, ViewPager.OnPageChangeListener {

    private TextView mTextMessage;

    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mDrawerToggle;


    private ImageView mTitleView = null;
    private Fragment mCurrentModalFragment = null;

    private TabPagerAdapter mPagerAdapter = null;
    private TabRootFragment mCurrentFragment = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitleView = findViewById(R.id.image_header_title);
        ImageLoader.Companion.getInstance().setActivity(this);
        Client.INSTANCE.setCurrentActivity(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        if(toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar supportActionBar = getSupportActionBar();
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        }

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer,
                R.string.empty,
                R.string.empty) {

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        };
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        MenuFragment menuFragment = new MenuFragment();
        menuFragment.setCloseListener(this);
        menuFragment.setSelectItemListener(this);
        getSupportFragmentManager().beginTransaction().add(R.id.fragmentMenu, menuFragment).commit();

        TabBar navigation = (TabBar) findViewById(R.id.navigation);
        navigation.addItem(getResources().getDrawable(R.mipmap.tab_home_on), getResources().getDrawable(R.mipmap.tab_home_off));

        mPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());

        if(Client.INSTANCE.getMyUser().getType() != 2) {
            navigation.addItem(getResources().getDrawable(R.mipmap.tab_homekit_on), getResources().getDrawable(R.mipmap.tab_homekit_off));
        }
        navigation.addItem(getResources().getDrawable(R.mipmap.tab_concierge_on), getResources().getDrawable(R.mipmap.tab_concierge_off));
        navigation.addItem(getResources().getDrawable(R.mipmap.tab_moresumaiful_on), getResources().getDrawable(R.mipmap.tab_moresumaiful_off));

        navigation.setListener(this);

        ViewPager pager = findViewById(R.id.root);
        pager.setOffscreenPageLimit(5);
        pager.addOnPageChangeListener(this);
        pager.setAdapter(mPagerAdapter);


        navigation.selectTab(0 , true);
        didClickTab(0);
    }

    @Override
    public void onBackPressed() {
        if(mCurrentFragment != null){
            if(mCurrentFragment.canPopBack()){
                mCurrentFragment.popFragment();
                return;
            }
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void didClickTab(int idx) {
        ViewPager pager = findViewById(R.id.root);
        TabRootFragment item = (TabRootFragment) mPagerAdapter.getItem(idx);
        if(pager.getCurrentItem() == idx){
            item.popToRootFragment();
        }
        else {
            pager.setCurrentItem(idx);
            item.willAppear();
        }
        mCurrentFragment = item;
        setTitleVisibility(!(item instanceof TopFragment));
        updateAccountTypeImage();
    }

    public void setPagingEnabled(boolean flg){
        TabPager pager = findViewById(R.id.root);
        pager.setPagingEnabled(flg);
    }

    public void setTitleVisibility(boolean flg){
        if(flg) mTitleView.setVisibility(View.VISIBLE);
        else mTitleView.setVisibility(View.INVISIBLE);
    }

    public void hideModal(){
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.detach(mCurrentModalFragment);
        fragmentTransaction.commit();

    }

    public void didLogout(){
        Intent next = new Intent(this, LoginActivity.class);
        startActivity(next);
        finish();

    }

    public void updateAccountTypeImage(){
        ImageView iv = findViewById(R.id.view_icon_type);
        if(mCurrentFragment instanceof TopFragment){
            int type = Client.INSTANCE.getMyUser().getType();
            switch(type){
                case 0:
                    iv.setImageDrawable(getResources().getDrawable(R.mipmap.icon_user_type_0));
                    break;
                case 1:
                    iv.setImageDrawable(getResources().getDrawable(R.mipmap.icon_user_type_1));
                    break;
                case 2:
                    iv.setImageDrawable(getResources().getDrawable(R.mipmap.icon_user_type_2));
                    break;
            }
        }
        else{
            iv.setImageDrawable(getResources().getDrawable(R.mipmap.header_logo));
        }
    }


    public void showUnsupportedMessage(){
        UnsupportedFragment fragment = new UnsupportedFragment();

        fragment.setDidTouchCallback(new Runnable() {
            @Override
            public void run() {
                hideModal();
            }
        });

        showModalFragment(fragment);
    }


    public void showLogoutCompleteMessage(){
        LogoutCompleteFragment fragment = new LogoutCompleteFragment();

        fragment.setDidTouchCallback(new Runnable() {
            @Override
            public void run() {
                hideModal();
                didLogout();
            }
        });

        showModalFragment(fragment);
    }


    public void showLogout(){
        LogoutFragment fragment = new LogoutFragment();

        fragment.setDidCLickFirstButton(new Runnable() {
            @Override
            public void run() {
                hideModal();
            }
        });

        fragment.setDidCLickSecondButton(new Runnable() {
            @Override
            public void run() {
                Client.INSTANCE.logout();
                showLogoutCompleteMessage();
            }
        });

        showModalFragment(fragment);
    }

    public void showDeleteAccountCompleteMessage(){
        DeleteAccountCompleteFragment fragment = new DeleteAccountCompleteFragment();
        fragment.setDidTouchCallback(new Runnable() {
            @Override
            public void run() {
                hideModal();
            }
        });
        showModalFragment(fragment);
    }

    public void showDeleteAccount(){

        DeleteAccountFragment fragment = new DeleteAccountFragment();

        fragment.setDidCLickFirstButton(new Runnable() {
            @Override
            public void run() {
                hideModal();
            }
        });

        fragment.setDidCLickSecondButton(new Runnable() {
            @Override
            public void run() {
                showDeleteAccountCompleteMessage();
            }
        });
        showModalFragment(fragment);
    }


    public void showPasswordChange(){
        PasswordChangeFragment fragment = new PasswordChangeFragment();
        fragment.setCancelCallback(new Runnable() {
            @Override
            public void run() {
                hideModal();
            }
        });
        showModalFragment(fragment);
    }

    public void showEmailChange(){
        EmailChangeFragment fragment = new EmailChangeFragment();
        fragment.setCancelCallback(new Runnable() {
            @Override
            public void run() {
                hideModal();
            }
        });
        showModalFragment(fragment);
    }

    private void showProfileEdit() {
        showMenuFragment(new ProfileEditFragment());
    }

    private void showFamilyKeyGeneration() {
        showMenuFragment(new FamilyKeyGenerationFragment());
    }

    private void showTermOfUse() {
        DisclaimerFragment fragment = new DisclaimerFragment();
        fragment.setTitle(getResources().getString(R.string.term_of_use));
        fragment.setContents(getResources().getString(R.string.term_of_use));
        if(Client.INSTANCE.getTermOfUseHtml() != null)
        fragment.setHtml(Client.INSTANCE.getTermOfUseHtml());
        else if(Client.INSTANCE.getTermOfUseURL() != null)
            fragment.setURL(Client.INSTANCE.getTermOfUseURL());
        else return;
        showMenuFragment(fragment);
    }

    private void showPrivacyPolicy() {
        DisclaimerFragment fragment = new DisclaimerFragment();
        fragment.setTitle(getResources().getString(R.string.privacy_policy));
        fragment.setContents(getResources().getString(R.string.privacy_policy));
        showMenuFragment(fragment);
    }

    private void showLicense() {
        DisclaimerFragment fragment = new DisclaimerFragment();
        fragment.setTitle(getResources().getString(R.string.menu_license));
        fragment.setContents(getResources().getString(R.string.menu_license));
        showMenuFragment(fragment);
    }

    public void showMenuFragment(SideMenuSubFragment fragment) {
        fragment.setCancelCallback(new Runnable() {
            @Override
            public void run() {
                hideModal();
            }
        });
        showModalFragment(fragment);
    }


    private void showModalFragment(Fragment fragment){
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.root_modal, fragment);
        fragmentTransaction.commit();

        mCurrentModalFragment = fragment;
    }

    @Override
    public void attemptToClose() {
        mDrawer.closeDrawers();
    }

    @Override
    public void selectItem(String name) {
        mDrawer.closeDrawers();
        if(name.equals(getResources().getString(R.string.menu_logout))){
            showLogout();
        }
        else if(name.equals(getResources().getString(R.string.menu_delete_account))){
            showDeleteAccount();
        }

        else if(name.equals(getResources().getString(R.string.menu_change_profile))){
            showProfileEdit();
        }
        else if(name.equals(getResources().getString(R.string.menu_family_key))){
            showFamilyKeyGeneration();
        }
        else if(name.equals(getResources().getString(R.string.term_of_use))){
            showTermOfUse();
        }
        else if(name.equals(getResources().getString(R.string.privacy_policy))){
            showPrivacyPolicy();
        }
        else if(name.equals(getResources().getString(R.string.menu_license))){
            showLicense();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        TabBar navigation = (TabBar) findViewById(R.id.navigation);
        navigation.selectTab(position, true);
        TabRootFragment item = (TabRootFragment) mPagerAdapter.getItem(position);
        item.willAppear();
        mCurrentFragment = item;
        setTitleVisibility(!(item instanceof TopFragment));
        updateAccountTypeImage();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
