package jp.sunnygem.sumaiful;

import android.content.Context;
import android.util.AttributeSet;

public class AspectRatioImageView extends URLImageView2 {
    public AspectRatioImageView(Context context) {
        super(context);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (getDrawable() == null)
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        else {
            int w = MeasureSpec.getSize(widthMeasureSpec);
            int h = w * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
            setMeasuredDimension(w, h);
        }
    }
}
