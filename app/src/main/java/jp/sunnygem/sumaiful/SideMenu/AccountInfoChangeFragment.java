package jp.sunnygem.sumaiful.SideMenu;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import jp.sunnygem.sumaiful.R;

public class AccountInfoChangeFragment extends Fragment {

    Runnable mCancelCallback;

    ImageButton mCloseButton;

    ViewTreeObserver.OnGlobalLayoutListener mKeyboardObserveListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            // navigation bar height
            int navigationBarHeight = 0;
            int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                navigationBarHeight = getResources().getDimensionPixelSize(resourceId);
            }

            // status bar height
            int statusBarHeight = 0;
            resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }

            // display window size for the app layout
            Rect rect = new Rect();

            getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);

            // screen height - (user app height + status + nav) ..... if non-zero, then there is a soft keyboard
            int keyboardHeight = getView().getRootView().getHeight() - (statusBarHeight + navigationBarHeight + rect.height());

            if (keyboardHeight <= 0) {
                    onHideKeyboard();
            } else {
                    onShowKeyboard();
            }
        }
    };


    public void onHideKeyboard(){
        mCloseButton.setVisibility(View.VISIBLE);

    }

    public void onShowKeyboard(){
        mCloseButton.setVisibility(View.INVISIBLE);
    }

    public void setCancelCallback(Runnable r){
        mCancelCallback = r;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_account_info_change, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCloseButton = view.findViewById(R.id.button_close);

        TextView username = view.findViewById(R.id.label_username);

        ImageButton close = view.findViewById(R.id.button_close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCancelCallback != null){
                    mCancelCallback.run();
                }
            }
        });


        Button change = view.findViewById(R.id.button_change);

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCancelCallback != null){
                    mCancelCallback.run();
                }
            }
        });



        getView().getViewTreeObserver().addOnGlobalLayoutListener(mKeyboardObserveListener);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mKeyboardObserveListener != null) {
            getView().getViewTreeObserver().removeGlobalOnLayoutListener(mKeyboardObserveListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
        if ( getView()  != null ) {
            InputMethodManager imm =
                    (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow( getView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS );
            getView().clearFocus();
        }
    }
}
