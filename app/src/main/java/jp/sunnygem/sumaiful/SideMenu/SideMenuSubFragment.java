package jp.sunnygem.sumaiful.SideMenu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageButton;

import jp.sunnygem.sumaiful.R;

public class SideMenuSubFragment extends Fragment {
    Runnable mCancelCallback;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageButton button = view.findViewById(R.id.button_close);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCancelCallback != null) mCancelCallback.run();
            }
        });

    }

    public void setCancelCallback(Runnable r){
        mCancelCallback = r;
    }

}
