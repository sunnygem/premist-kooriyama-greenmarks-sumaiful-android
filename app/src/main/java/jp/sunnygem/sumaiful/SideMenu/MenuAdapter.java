package jp.sunnygem.sumaiful.SideMenu;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.R;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {


    class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View itemView) {
            super(itemView);

        }
        void bindData(MenuItem item){

            TextView title = itemView.findViewById(R.id.label_title);
            title.setText(item.getTitle());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Runnable tapCallback = item.getTapCallback();
                    if(tapCallback != null){
                        tapCallback.run();
                    }
                }
            });
        }
    }

    class MenuItem{
        private String mTitle;
        private Runnable mTapCallback;

        MenuItem(String title, Runnable callback){
            mTitle = title;
            mTapCallback = callback;
        }
        String getTitle(){return mTitle;};
        Runnable getTapCallback(){return mTapCallback;};

    }

    List<MenuItem> mItems = new ArrayList<>();


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_recycle_menu, parent, false);
        ViewHolder viewHolder = new ViewHolder(inflate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(mItems.get(position));
    }


    void addItem(String title, Runnable callback){
        mItems.add(new MenuItem(title, callback));
        notifyDataSetChanged();
    }

    void clear(){
        mItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }



}
