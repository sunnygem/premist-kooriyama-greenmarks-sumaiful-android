package jp.sunnygem.sumaiful.SideMenu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.R;


public class MenuFragment extends Fragment {


    MenuAdapter mAdapter = new MenuAdapter();

    public interface CloseMenuListener{
        void attemptToClose();
    }

    public interface SelectItemListener{
        void selectItem(String name);
    }

    private CloseMenuListener mCloseListener = null;
    private SelectItemListener mSelectItemListener = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_menu, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter.clear();

        String[] names = {
                getResources().getString(R.string.term_of_use),
                getResources().getString(R.string.menu_logout),
        };

        for(String name : names){
            mAdapter.addItem(name, new Runnable() {
                @Override
                public void run() {
                    if(mSelectItemListener != null) mSelectItemListener.selectItem(name);
                }
            });

        }


        View view1 = getView();
        ImageView userTypeView = view1.findViewById(R.id.image_user_type);
        int type = Client.INSTANCE.getMyUser().getType();
        switch(type){
            case 0:
                userTypeView.setImageDrawable(getResources().getDrawable(R.mipmap.icon_user_type_0));
                break;
            case 1:
                userTypeView.setImageDrawable(getResources().getDrawable(R.mipmap.icon_user_type_1));
                break;
            case 2:
                userTypeView.setImageDrawable(getResources().getDrawable(R.mipmap.icon_user_type_2));
                break;
        }


        AppCompatImageButton button = view1.findViewById(R.id.button_close);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCloseListener != null){
                    mCloseListener.attemptToClose();
                }
            }
        });

        RecyclerView list = view1.findViewById(R.id.list_menu);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());

        list.setLayoutManager(llm);
        list.setAdapter(mAdapter);
    }

    public void setCloseListener(CloseMenuListener listener){
        mCloseListener = listener;
    }
    public void setSelectItemListener(SelectItemListener listener){
        mSelectItemListener = listener;
    }
}
