package jp.sunnygem.sumaiful.SideMenu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

import jp.sunnygem.sumaiful.Client;
import jp.sunnygem.sumaiful.R;

public class DisclaimerFragment extends SideMenuSubFragment {


    String mTitle = "";
    String mContents = "";
    String mURL = "";
    String mHtml = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_fragment_disclaimer, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView userNameView = view.findViewById(R.id.label_username);
        userNameView.setText(Client.INSTANCE.getMyUser().getName());

        TextView titleView = view.findViewById(R.id.label_title);

        titleView.setText(mTitle);
        WebView webView = view.findViewById(R.id.view_web);
        if(mHtml != null && !mHtml.isEmpty()){
            webView.loadData(mHtml, "text/html; charset=utf-8", "utf-8");
        }
        else{
            webView.loadUrl(mURL);
        }

    }

    public void setURL(String URL) {
        this.mURL = URL;
    }

    public void setHtml(String html) {
        this.mHtml = html;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getContents() {
        return mContents;
    }

    public void setContents(String contents) {
        mContents = contents;
    }
}
