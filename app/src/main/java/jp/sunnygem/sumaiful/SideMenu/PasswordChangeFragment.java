package jp.sunnygem.sumaiful.SideMenu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import jp.sunnygem.sumaiful.R;

public class PasswordChangeFragment extends AccountInfoChangeFragment {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        TextView message = view.findViewById(R.id.label_message);
        message.setText(R.string.label_password_change_message);

        TextView currentTitle = view.findViewById(R.id.label_current);
        currentTitle.setText(R.string.label_current_password);

        EditText currentInput = view.findViewById(R.id.input_current);

        TextView newTitle = view.findViewById(R.id.label_new);
        newTitle.setText(R.string.label_new_password);

        EditText newInput = view.findViewById(R.id.input_new);



    }
}
