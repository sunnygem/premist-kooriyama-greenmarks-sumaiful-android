package jp.sunnygem.sumaiful;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import jp.sunnygem.sumaiful.Tab.Communication.CommunicationFragment;
import jp.sunnygem.sumaiful.Tab.Concierge.ConciergeFragment;
import jp.sunnygem.sumaiful.Tab.HomeKit.HomeKitFragment;
import jp.sunnygem.sumaiful.Tab.Sumaiful.SumaifulFragment;
import jp.sunnygem.sumaiful.Tab.Top.TopFragment;

public class TabPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragments = new ArrayList<>();

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragments.add(new TopFragment());
        if(Client.INSTANCE.getMyUser().getType() != 2) {
            mFragments.add(new HomeKitFragment());
        }
        mFragments.add(new ConciergeFragment());
        mFragments.add(new SumaifulFragment());

    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }
}
