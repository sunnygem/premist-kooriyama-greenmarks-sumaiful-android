package jp.sunnygem.sumaiful

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import android.content.Context.NOTIFICATION_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.support.v4.app.NotificationCompat
import android.content.Intent
import android.app.NotificationChannel
import android.os.Build






class DeviceTokenListenerService: FirebaseMessagingService() {

    override fun onNewToken(s: String?) {
        super.onNewToken(s)
        Log.d("NEW_TOKEN",s);
    }

    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        p0?.notification?.let{
            val notificationBuilder = NotificationCompat.Builder(this, createNotificationChannel(this) ?: "")
                .setSmallIcon(R.mipmap.icon_notification)
                .setContentTitle(it.title ?: "")
                .setSubText(it.body)
                .setStyle(NotificationCompat.BigTextStyle().bigText(it.body).setBigContentTitle("willing"))
            .setContentIntent(pendingIntent)

            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(0, notificationBuilder.build())
        }

    }

    fun createNotificationChannel(context: Context): String? {

        // NotificationChannels are required for Notifications on O (API 26) and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // The id of the channel.
            val channelId = "Channel_id"

            // The user-visible name of the channel.
            val channelName = "Application_name"
            // The user-visible description of the channel.
            val channelDescription = "Application_name Alert"
            val channelImportance = NotificationManager.IMPORTANCE_DEFAULT
            val channelEnableVibrate = true
            //            int channelLockscreenVisibility = Notification.;

            // Initializes NotificationChannel.
            val notificationChannel = NotificationChannel(channelId, channelName, channelImportance)
            notificationChannel.description = channelDescription
            notificationChannel.enableVibration(channelEnableVibrate)
            //            notificationChannel.setLockscreenVisibility(channelLockscreenVisibility);

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)

            return channelId
        } else {
            // Returns null for pre-O (26) devices.
            return null
        }
    }


}