//
// Created by occlusion on 4/3/19.
//


#include <iostream>
#include <string>
#include <vector>
#include <openssl/ssl.h>

#include <openssl/evp.h>
#include <openssl/hmac.h>

#include <openssl/kdf.h>
#include <openssl/bio.h>
#include <assert.h>

namespace sumaiful{
    size_t calcDecodeLength(const char* b64input) { //Calculates the length of a decoded string
        size_t len = strlen(b64input),
                padding = 0;

        if (b64input[len-1] == '=' && b64input[len-2] == '=') //last two chars are =
            padding = 2;
        else if (b64input[len-1] == '=') //last char is =
            padding = 1;

        return (len*3)/4 - padding;
    }

    std::vector<unsigned char> Base64Decode(const std::string &m64Message) { //Decodes a base64 encoded string
        std::vector<unsigned char> data;
        BIO *bio, *b64;

        size_t decodeLen = calcDecodeLength(m64Message.c_str());
        data.resize(decodeLen+1);
        data[decodeLen] = '\0';

        bio = BIO_new_mem_buf(m64Message.c_str(), -1);
        b64 = BIO_new(BIO_f_base64());
        bio = BIO_push(b64, bio);

        BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Do not use newlines to flush buffer
        int length = BIO_read(bio, data.data(), (int)m64Message.size());
        assert(length == decodeLen); //length should equal decodeLen, else something went horribly wrong
        BIO_free_all(bio);
        data.resize(length);
        return data;
    }

    std::string Base64Encode(const std::vector<unsigned char> &data) { //Encodes a binary safe base 64 string
        BIO *bio, *b64;
        BUF_MEM *bufferPtr;

        b64 = BIO_new(BIO_f_base64());
        bio = BIO_new(BIO_s_mem());
        bio = BIO_push(b64, bio);

        BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Ignore newlines - write everything in one line
        BIO_write(bio, data.data(), (int)data.size());
        BIO_flush(bio);
        BIO_get_mem_ptr(bio, &bufferPtr);
        BIO_set_close(bio, BIO_NOCLOSE);
        const char *d = (*bufferPtr).data;
        std::string result = std::string(d, bufferPtr->length);
        BIO_free_all(bio);

        return result;
    }

    std::vector<unsigned char> encode(const std::string &data, const std::vector<unsigned char> &iv, const std::string &key){

        std::vector<unsigned char> encrypted(16 + data.size()*2);
        auto cipher_type = EVP_get_cipherbyname("AES-256-CBC");
        auto ctx = EVP_CIPHER_CTX_new();

        int ivLen = EVP_CIPHER_iv_length(cipher_type);
        bool encode = true;
        EVP_CipherInit_ex(ctx, cipher_type, NULL, NULL, NULL, encode);
        unsigned char *piv = (unsigned char *)malloc(ivLen);
        memset(piv, 0, ivLen);
        size_t sz = iv.size();
        if(sz > ivLen) sz = ivLen;
        memcpy(piv, &iv[0], sz);
        int keyLen = EVP_CIPHER_key_length(cipher_type);
        unsigned char *pkey = NULL;
        if(keyLen >= key.size()){
            pkey = (unsigned char *)malloc(keyLen);
            memset(pkey, 0, keyLen);
            memcpy(pkey, &key[0], key.size());
            //        EVP_CIPHER_CTX_set_key_length(ctx, keyLen);
        }
        else{
            pkey = (unsigned char *)malloc(key.size());
            memcpy(pkey, key.data(), key.size());
            EVP_CIPHER_CTX_set_key_length(ctx, (int)key.size());
        }
        EVP_CipherInit_ex(ctx, NULL, NULL, pkey, piv, encode);

        int outl = (int)encrypted.size();
        EVP_CipherUpdate(ctx, &encrypted[0], &outl, (unsigned char *)data.data(), (int)data.size());
        int size = (int)(encrypted.size() - outl);
        EVP_EncryptFinal(ctx, &encrypted[outl], &size);
        int total = outl+size;

        EVP_CIPHER_CTX_cleanup(ctx);
        EVP_CIPHER_CTX_free(ctx);
        free(pkey);
        free(piv);
        encrypted.resize(total);
        return encrypted;

    }



    std::vector<unsigned char> decode(const std::vector<unsigned char> &data, std::vector<unsigned char> &iv, const std::string &key){

        std::vector<unsigned char> encrypted(16 + data.size()*2);
        auto cipher_type = EVP_get_cipherbyname("AES-256-CBC");
        auto ctx = EVP_CIPHER_CTX_new();
        //    EVP_CIPHER_mode(cipher_type);


        int ivLen = EVP_CIPHER_iv_length(cipher_type);
        bool encode = false;
        EVP_CipherInit_ex(ctx, cipher_type, NULL, NULL, NULL, encode);
        unsigned char *piv = (unsigned char *)malloc(ivLen);
        memset(piv, 0, ivLen);
        size_t sz = iv.size();
        if(sz > ivLen) sz = ivLen;
        memcpy(piv, &iv[0], sz);

        int keyLen = EVP_CIPHER_key_length(cipher_type);
        unsigned char *pkey = NULL;
        if(keyLen >= key.size()){
            pkey = (unsigned char *)malloc(keyLen);
            memset(pkey, 0, keyLen);
            memcpy(pkey, &key[0], key.size());
        }
        else{
            pkey = (unsigned char *)malloc(key.size());
            memset(pkey, 0, key.size());
            memcpy(pkey, key.data(), key.size());
            EVP_CIPHER_CTX_set_key_length(ctx, (int)key.size());
        }
        EVP_CipherInit_ex(ctx, NULL, NULL, pkey, piv, encode);

        int outl = (int)encrypted.size();
        EVP_CipherUpdate(ctx, &encrypted[0], &outl, (unsigned char *)data.data(), (int)data.size());
        int size = (int)(encrypted.size() - outl);
        EVP_DecryptFinal(ctx, &encrypted[outl], &size);
        int total = outl+size;

        EVP_CIPHER_CTX_cleanup(ctx);
        EVP_CIPHER_CTX_free(ctx);
        free(pkey);
        free(piv);
        encrypted.resize(total);
        return encrypted;
    }

    std::vector<unsigned char> hkdf(const std::string &key, const std::string &info, const std::vector<unsigned char> &salt){
        EVP_PKEY_CTX *pctx;
        static const int osz = 32;
        std::vector<unsigned char> out(osz);
        size_t outlen = osz;
        pctx = EVP_PKEY_CTX_new_id(EVP_PKEY_HKDF, NULL);
        if (EVP_PKEY_derive_init(pctx) >= 0)
            if (EVP_PKEY_CTX_set_hkdf_md(pctx, EVP_sha256()) >= 0)
                if (EVP_PKEY_CTX_set1_hkdf_salt(pctx, salt.data(), salt.size()) >= 0)
                    if (EVP_PKEY_CTX_set1_hkdf_key(pctx, key.data(), key.size()) >= 0)
                        if (EVP_PKEY_CTX_add1_hkdf_info(pctx, info.data(), info.size()) >= 0)
                            if (EVP_PKEY_derive(pctx, &out[0], &outlen) >= 0){}
        assert(!out.empty());
        return out;
    }

    std::string encrypt(const std::string &data, const std::string &password, const std::string &iv, const std::string &salt, const std::string &info){
        auto ivd = Base64Decode(iv);
        auto saltd = Base64Decode(salt);
        std::vector<unsigned char> key = hkdf(password, info, saltd);
        std::string k = std::string((char *)key.data(), key.size());
        auto r = encode(data, ivd, k);
        return Base64Encode(r);
    }
    std::string decrypt(const std::string &data, const std::string &password, const std::string &iv, const std::string &salt, const std::string &info){
        auto ivd = Base64Decode(iv);
        auto saltd = Base64Decode(salt);
        std::vector<unsigned char> key = hkdf(password, info, saltd);
        std::string k = std::string((char *)key.data(), key.size());
        auto d = Base64Decode(data);
        auto r = decode(d, ivd, k);
        return std::string((const char *)r.data(), r.size());

    }
    void InitOpenSSL(){
        static bool AddedAlgorithms = false;
        if(!AddedAlgorithms) {
            OpenSSL_add_all_algorithms();
            AddedAlgorithms = true;
        }
    }

}





#ifdef __cplusplus
extern "C" {
#endif

#include <jni.h>

JNIEXPORT jstring JNICALL Java_jp_sunnygem_sumaiful_Client_Encrypt
  (JNIEnv *env, jobject thisObj, jstring data, jstring password, jstring iv, jstring salt, jstring info) {
    const char *cdata = env->GetStringUTFChars(data, 0);
    const char *cpassword = env->GetStringUTFChars(password, 0);
    const char *civ = env->GetStringUTFChars(iv, 0);
    const char *csalt = env->GetStringUTFChars(salt, 0);
    const char *cinfo = env->GetStringUTFChars(info, 0);


    sumaiful::InitOpenSSL();


    auto r = sumaiful::encrypt(cdata, cpassword, civ, csalt, cinfo);

    env->ReleaseStringUTFChars(data, cdata);
    env->ReleaseStringUTFChars(data, cpassword);
    env->ReleaseStringUTFChars(data, civ);
    env->ReleaseStringUTFChars(data, csalt);
    env->ReleaseStringUTFChars(data, cinfo);

    return env->NewStringUTF(r.c_str());
}

#ifdef __cplusplus
}
#endif